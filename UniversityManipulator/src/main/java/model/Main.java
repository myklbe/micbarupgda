package model;

import helper.QueryHelper;
import resolver.StudentResolver;

import java.util.*;

/**
 * Created by Lenovo on 06.07.2017.
 */
public class Main {
    public static void main(String[] args) {



        Scanner sc = new Scanner(System.in);
        StudentResolver rs = new StudentResolver();
        Map<String, String> pops = new HashMap<>();
        pops.put("id", null);
        pops.put("name", "Robert");
        pops.put("lastName", "Hojnacki");
        List<String> strin = new ArrayList<>();
        strin.add(pops.get("id"));
        strin.add(pops.get("name"));

//        System.out.println(pops.get("id") == null);


        System.exit(0);
        System.out.println(" 1.Pokaż rekordy " + "\n " +
                "2.Dodaj rekord" + "\n " +
                "3.Usuń rekord [id=]" + "\n " +
                "4.Nadpisz rekord [id=]" + "\n " +
                "5.Koniec ");
        while(sc.hasNext()){
            String input = sc.nextLine();
            if(input.equals("1")){
                List<Student> studentList = rs.getList();
                for (Student s : studentList){
                    System.out.println(s.getId() +" "+ s.getName()+" "+ s.getLastName());
                }
            } else if (input.equals("2")) {
                System.out.println("Podaj imię: ");
                String name =  sc.nextLine();
                System.out.println("Podaj nazwisko: ");
                String surname = sc.nextLine();
                Map<String, String> newStudent = new HashMap<>();
                newStudent.put("name", name);
                newStudent.put("lastName", surname);
                System.out.println(rs.insertOrUpdate(newStudent));
            } else if (input.equals("3")){
                System.out.println("Podaj id rekordu który chcesz usunąć");
                int nextInput = sc.nextInt();
                System.out.println(rs.delete(nextInput));
            } else if (input.equals("4")){
                System.out.println("Podaj id które chcesz uzupełnić/zmienić: ");
                String id = sc.nextLine();
                System.out.println("Podaj nazwę kolumny: ");
                String columnName = sc.nextLine();
                System.out.println("Podaj nowy wpis: ");
                String newValue = sc.nextLine();
                Map<String, String> update = new HashMap<>();
                update.put("id", id);
                update.put(columnName, newValue);
                System.out.println(rs.insertOrUpdate(update));
            } else if (input.equals("5")){
                System.exit(0);
            }
        }

//
//



    }

}
