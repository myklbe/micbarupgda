package resolver;

import model.Student;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Lenovo on 06.07.2017.
 */
public abstract class AbstractResolver<T> {

    public abstract T get(int id, Map<String, String> props) throws SQLException;

    public abstract List<T> getList() throws SQLException;

    public abstract boolean delete (int id) throws SQLException;

    public abstract boolean insertOrUpdate (Map<String, String> params) throws SQLException;

}
