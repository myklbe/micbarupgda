package resolver;

import connector.DatabaseConnector;
import helper.QueryHelper;
import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Lenovo on 06.07.2017.
 */
public class StudentResolver extends AbstractResolver<Student> {


    @Override
    public Student get(int id, Map<String, String> props) {
        Connection c = null;
        Student student = new Student();
        try {
            c = DatabaseConnector.getInstance().getConnection();
            String query = QueryHelper.select("student",props) + id;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setLastName(rs.getString("lastName"));
            }
            return student;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public List<Student> getList() {
        Connection c = null;
        List<Student> studentList = new ArrayList<>();
        try {

            c = DatabaseConnector.getInstance().getConnection();
            String query = QueryHelper.selectAll("student");
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("lastName");
                Student student = new Student(id, name, surname);
                studentList.add(student);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentList;
    }


    @Override
    public boolean delete(int id) {
        Connection c = null;
        try {
            c = DatabaseConnector.getInstance().getConnection();
            String query = QueryHelper.delete("student", id);
            Statement s = c.createStatement();
            int check = s.executeUpdate(query);
            return check > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

//    @Override
//    public boolean insert(Map<String, String> params)  {
//        Connection c = null;
//        try {
//            c = DatabaseConnector.getInstance().getConnection();
//           String query = QueryHelper.insert("student", params);
//           Statement s = c.createStatement();
//            return  s.executeUpdate(query)> 0;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    @Override
//    public boolean update(int id, Map<String, String> params) {
//        Connection c = null;
//        try {
//            c = DatabaseConnector.getInstance().getConnection();
//
//            String query = QueryHelper.update("student", params) + id;
//            Statement s = c.createStatement();
//
//            int check = s.executeUpdate(query);
//            return check > 0;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
    @Override
    public boolean insertOrUpdate(Map<String, String> params){

        Connection c = null;
        int check = 0;
        try {
            c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();

            if(params.containsKey("id") && params.get("id") != null) {
                String queryUpdate = QueryHelper.update("student", params);
                check = s.executeUpdate(queryUpdate);
                return check > 0;
            } else {
                check = s.executeUpdate(QueryHelper.insert("student", params));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

}
