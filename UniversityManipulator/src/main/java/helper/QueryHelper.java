package helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Lenovo on 07.07.2017.
 */
public class QueryHelper {
    public static String update(String tableName, Map<String, String> props) {
        String q = "UPDATE `" + tableName + "` SET";
        String queryUpdate = "";
        int id = Integer.valueOf(props.get("id"));
        props.remove("id");

        for (Map.Entry<String, String> e : props.entrySet()) {
            queryUpdate += " `" + e.getKey() + "` = '" + e.getValue() + "',";
        }
        q += queryUpdate.substring(0, queryUpdate.length() - 1);
        q += " WHERE `id` = " + id;
        return q;
    }

    public static String insert(String tableName, Map<String, String> props) {
        // INSERT INTO `tableName` VALUES (values)
        String ret = "INSERT INTO `" + tableName + "` (";
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        String id = props.get("id");
        props.remove("id");
        for (Map.Entry<String, String> e : props.entrySet()) {
            keys.add(e.getKey());
            values.add(e.getValue());
        }
        String key = "";
        for (String s : keys) {
            key += s + ", ";
        }
        ret += key.substring(0, key.length() - 2) + ", id" + ") VALUES (";
        String value = "";
        for (String s : values) {
            value += "'" + s + "', ";
        }
        ret += value.substring(0, value.length() - 2) + ", null" + ")";
        return ret;


    }

    public static String delete(String tableName, int id) {
        String query = "DELETE FROM `" + tableName + "` WHERE `id`= " + id;
        return query;
    }
    public static String select(String tablename, Map<String, String> props){
        // SELECT column(props.key) FROM tablename WHERE id = id;
        return "SELECT " + props.get("columnName") + " FROM " +tablename+ " WHERE id = ";
    }
    public static String selectAll(String tablename){
        return "SELECT * FROM " + tablename;
    }
}
