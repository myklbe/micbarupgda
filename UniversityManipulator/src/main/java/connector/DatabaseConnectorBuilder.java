package connector;

/**
 * Created by Lenovo on 06.07.2017.
 */
public class DatabaseConnectorBuilder {
    private String dbhost = "localhost";
    private String dbname;
    private String dbuser = "root";
    private String dbpass = "root";
    private int dbport = 3306;

    public DatabaseConnectorBuilder(){}

    public String hostname() {
        return dbhost;
    }

    public String database() {
        return dbname;
    }

    public String username() {
        return dbuser;
    }

    public String password() {
        return dbpass;
    }

    public int port() {
        return dbport;
    }

    public DatabaseConnectorBuilder hostname(String dbhost) {
        this.dbhost = dbhost;
        return this;
    }

    public DatabaseConnectorBuilder database(String dbname) {
        this.dbname = dbname;
        return this;
    }

    public DatabaseConnectorBuilder username(String dbuser) {
        this.dbuser = dbuser;
        return this;
    }

    public DatabaseConnectorBuilder password(String dbpass) {
        this.dbpass = dbpass;
        return this;
    }

    public DatabaseConnectorBuilder port(int dbport) {
        this.dbport = dbport;
        return this;
    }


}
