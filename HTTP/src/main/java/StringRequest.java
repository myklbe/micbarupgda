import java.util.Map;

/**
 * Created by Lenovo on 14.07.2017.
 */
public class StringRequest {
    public String RequestURI(String url, Map<String, String> params){
        String mapUri = "";
        for(Map.Entry<String, String> keys : params.entrySet()){
            mapUri += keys.getKey() + "=" + keys.getValue() + "&";
        }

        String uri = url + "?" + mapUri.substring(0,mapUri.length()-1);
        return uri;
    }


}
