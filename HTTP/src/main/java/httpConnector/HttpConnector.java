package httpConnector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Lenovo on 14.07.2017.
 */
public class HttpConnector {

    private URL obj;
    private HttpURLConnection con;
    private String ua;

    public HttpConnector(){}

    public HttpConnector(URL obj, HttpURLConnection con, String ua) {
        this.obj = obj;
        this.con = con;
        this.ua = ua;
    }

    public String sendGET(String url) throws IOException {
        obj = new URL(url);
        con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", this.ua);


        int responseCOde = con.getResponseCode();
        String result = "";
        if(responseCOde == 200){
            InputStream is = con.getInputStream();
            InputStreamReader reader = new InputStreamReader(is);
            Scanner sc = new Scanner(reader);

            while(sc.hasNextLine()){
                result += sc.nextLine();
            }
            sc.close();
        }

        return result;
    }
    public String sentPOST(String url, String params) throws IOException {
        obj = new URL(url);
        con = (HttpURLConnection) obj.openConnection();


        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", this.ua);

        con.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(con.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();

        Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
        String lines = "";
        while(sc.hasNextLine()){
            lines += sc.nextLine();
        }
        sc.close();

        return lines;
    }

}
