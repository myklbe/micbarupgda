package httpConnector;

/**
 * Created by Lenovo on 14.07.2017.
 */
public enum HttpMethod {
    GET, PUT, PATCH, POST, DELETE, OPTIONS;

}
