package java;

import java.util.Arrays;

public class Sort {

    public static void main(String[] args) {
        int[] arr = {1,2,2,3,1,3,1,1};

        System.out.println(Arrays.toString(reverseArray(arr)));

    }
    private static int[] reverseArray(int[] arr){
        for (int i = 0; i < arr.length/2; i++) {
            int a = arr[i];
            arr[i] = arr[arr.length - (i+1)];
            arr[arr.length - (i+1)] = a;
        }
        return arr;
    }


}
