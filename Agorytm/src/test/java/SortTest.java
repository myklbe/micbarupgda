import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class SortTest {

    @Test
    public void sort() throws Exception{

        int data[] = {1,6,3,58,8,4,9};
        Sort.mergeSort(data);
        Assertions.assertThat(data).isSorted();

    }

}
