import java.util.Arrays;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Sort {

    public static int[] bubbleSort(int[] array) {
        int value = 0;
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[j - 1] > array[j]) {
                    value = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = value;
                }
            }
        }
        return array;
    }

    public static int[] insertSort(int[] array) {
        int i, j, newValue;
        for (i = 1; i < array.length; i++) {
            newValue = array[i];
            j = i;
            while (j > 0 && array[j - 1] > newValue) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = newValue;
        }
        return array;
    }

    public static int[] choiceSort(int[] data) {
// za kazdym razem szukasz najmniejszego elementu i zamieniasz go z elementem od 0 indeks
        for (int i = 0; i < data.length; i++) {
            int min = data[i];
            int minIndex = i;
            for (int j = i; j < data.length; j++) {
                if (min > data[j]) {
                    min = data[j];
                    minIndex = j;
                }
            }
            int buffer = data[i];
            data[i] = min;
            data[minIndex] = buffer;
        }
        return data;
    }

    public static void mergeSort(int[] data) {
        if (data.length > 1) {
            int middle = data.length / 2;
            int[] left = copy(0, middle - 1, data);
            int[] right = copy(middle, data.length - 1, data);
            mergeSort(left);
            mergeSort(right);
            merge(data, left, right);


        }
    }

    private static void merge(int[] data, int[] left, int[] right) {
        int b = 0;
        for (int i = 0; i < left.length; i++) {
            data[b] = (left[i] <= right[i]) ? left[i] : right[i];
            data[b + 1] = (data[b] != left[i]) ? left[i] : right[i];
            b += 2;
        }
        data[data.length - 1] =
                left.length != right.length ? right[right.length - 1] :
                        data[data.length - 1];
    }

    private static int[] copy(int start, int end, int[] data) {
        int j = 0;
        int[] copy = new int[end - start + 1];
        for (int i = start ; i <= end ; i++) {
            copy[j++] = data[i];
        }
        return copy;

    }


    public static void main(String[] args) {

        int data[] = new int[7];
        int[] left = {1, 3, 7,};
        int[] right = {2, 5, 6, 7};
        Sort.merge(data, left, right);
        System.out.println(Arrays.toString(data));

    }


}
