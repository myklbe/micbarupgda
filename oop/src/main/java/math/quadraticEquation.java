package math;

public class quadraticEquation {
	private double a;
	private double b;
	private double c;
	
	public quadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		
		
	}
	public double calcDelta() {
		return (b*b) - (4*(a*c));
	}
	public double calcX1() {
		return (-b + Math.sqrt(calcDelta()))/(2*a);
	}
	public double calcX2() {
		return (-b - Math.sqrt(calcDelta()))/(2*a);
}
}

