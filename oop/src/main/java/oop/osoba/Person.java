package oop.osoba;

public class Person {
	// Stan
	private String imie;     // w javie wszelkie dane powinnny by� ukryte przed urzytkownikiem klasy(programist�)
	private int wiek;
	
	// Konstruktor
	public Person(String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}
	public Person(){
		
	}
	// Zachowanie
	public void przedstawSie(){
		System.out.println(imie + " (" + wiek + ")");
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	public String getImie() {
		return imie;
	}
	public int getWiek() {
		return wiek;
	}
}
