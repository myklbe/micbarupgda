package oop.osoba;

public class PersonMain {

	public static void main(String[] args){
		
		Person jan = new Person("Jan", 23);
		jan.przedstawSie();
		Person adam = new Person("Adam", 25);
		adam.przedstawSie();
		adam.setImie("Jacek");
		adam.setWiek(34);
		Person karol = new Person();
		
		
		
		System.out.println(adam.getImie() +" " + adam.getWiek());
		
		new Person();
		
		
		
		//Person jan = new Person(); // Tworzenie instatncji obiektu
		//jan.imie = "Jan";
		//jan.wiek = 23;
		
		
	}
}
