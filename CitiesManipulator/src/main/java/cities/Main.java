package cities;


import cities.connector.DatabaseConnector;
import cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

//        delete("city", "id", "2");
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "INSERT INTO `city` VALUES (id, ?)";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1,"Kraków");
            ps.close();

        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }

//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("INSERT INTO `city` VALUES (id, 'Wrocław')");
//            s.close();
//
//        } catch(SQLException e) {
//            System.out.println(e.getMessage());
//        }


        List<City> listOfCities = new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `city`";

            Statement s = connection.createStatement();
            ResultSet rs =  s.executeQuery(query);

            while(rs.next()) {
                listOfCities.add(new City(rs.getInt("id"), rs.getString("city")));
            }

            rs.close();
            s.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(City c : listOfCities){
            System.out.println("> " +c.getCity());
        }


    }

    private static void delete(String tableName, String columnName, String setValue) {
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `"
                    + tableName
                    + "` WHERE `"
                    + columnName
                    +"` = `"
                    + setValue
                    + "`");
            s.close();

        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }


}
