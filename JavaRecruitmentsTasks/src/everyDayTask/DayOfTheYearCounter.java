package everyDayTask;

public class DayOfTheYearCounter {

    // podaje date w formacie dd-MM-yyyy i ma policzyć który dzień w roku.


    private String year;

    public DayOfTheYearCounter(String year) {
        this.year = year;
    }

    private String[] fullYear() {
        return year.split("-");
    }

    private boolean isLeap() {
        int onlyYear = Integer.valueOf(fullYear()[2]);
        if (onlyYear % 4 == 0) {
            if (onlyYear % 100 != 0) {
                return true;
            } else if (onlyYear % 400 == 0) {
                return true;
            }
        }
        return false;
    }

    public int whichDayOfTheYear() {
        int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int month = Integer.valueOf(fullYear()[1]);
        int dayOfTheYear = Integer.valueOf(fullYear()[0]);
        for (int i = month - 1; i > 0; i--) {
            dayOfTheYear += months[i - 1];
        }
        if (isLeap() && month > 2) {
            dayOfTheYear += 1;
        } else if (isLeap() && month == 2 && Integer.valueOf(fullYear()[0]) == 29) {
            dayOfTheYear += 1;
        }
        return dayOfTheYear;
    }

    public static void main(String[] args) {

        DayOfTheYearCounter counter = new DayOfTheYearCounter("31-12-2012");
        System.out.println(counter.whichDayOfTheYear());

    }

}
