package everyDayTask.javaExam.excercises;

import javax.swing.text.Document;
import java.io.*;
import java.util.*;

public class ShortExcercises {

    public static void main(String[] args) {

        try {
            System.out.println(sumNumbersFromFile("C:\\Users\\Lenovo\\workspace\\JavaRecruitmentsTasks\\src\\resources\\numbers.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String fromString(String str) {
        Set<Character> characterSet = new HashSet<>();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            if (!characterSet.contains(str.charAt(i))) {
                characterSet.add(str.charAt(i));
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }

    public static int policzSilnie(int n) {
        if (n < 2) {
            return 1;
        } else
            return n * policzSilnie(n - 1);
    }

    public static void fizzBuzz(int n) {
        if (n % 3 == 0 && n % 5 == 0) {
            System.out.println("FizzBuzz");
        } else if (n % 3 == 0) {
            System.out.println("Fizz");
        } else if (n % 5 == 0) {
            System.out.println("Buzz");
        } else
            System.out.println(n);
    }

    public static void drawTriange(int n) {
        String print = "";
        for (int i = 0; i < n; i++) {
            print += 'o';
            System.out.println(print);
        }
    }

    public static void separateNumbers(String value) {
        int counter = 0;
        String str = "";
        for (int i = 0; i < value.length(); i++) {
            if (Character.isDigit(value.charAt(i))) {
                counter++;
            } else {
                str += value.charAt(i);
            }
        }
        System.out.println(str + "  " + counter);
    }

    public static void averageMinusMultiplePlus(int... args) {
        int avg = 0;
        int multi = 1;
        int counter = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i] < 0) {
                avg += args[i];
                counter++;
            } else if (args[i] > 0) {
                multi = multi * args[i];
            }
        }
        avg = avg / counter;
        System.out.println("Srednia ujemnych: " + avg + " iloczyn dodatnich: " + multi);
    }

    static void findFirstPair(int[] arr, int sum) {
        int head = 0;
        int tail = arr.length - 1;
        while (head != tail) {
            int indexSum = arr[head] + arr[tail];
            if (indexSum > sum) {
                tail--;
            } else if (indexSum < sum) {
                head++;
            } else if (indexSum == sum) {
                System.out.println(arr[head] + "   " + arr[tail]);
                break;
            }
            if (head == tail) {
                System.out.println("There is no pair");
            }
        }
    }

    static void buildTriangleFromRight(int n) {
        String out1 = "";
        String out2 = "";
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < n; j++) {
                out1 += "  ";
                out2 += " *";
            }
            out1 = out1.substring(i * 2);
            out1 += out2.substring((n - i) * 2);
            System.out.println(out1);
            out1 = "";
            out2 = "";
        }
    }

    static void buildBox(int n) {
        String out1 = "";
        String out2 = "";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                out1 += "O";
                out2 += " ";
            }
            if (i > 0 && i < n - 1) {
                out1 = "O" + out2.substring(1, out2.length() - 1) + "O";
                System.out.println(out1);
            } else {
                System.out.println(out1);
            }
            out1 = "";
            out2 = "";
        }
    }

    static void buildChristmasTree(int n) {
        String out1 = "";
        String out2 = "";
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < n; j++) {
                out1 += " ";
                out2 += " *";

            }
            out1 = out1.substring(i);
            out1 += out2.substring((n - i) * 2);
            System.out.println(out1);
            out1 = "";
            out2 = "";
        }
    }

    static int[] fibbonacciByIteration(int n) {
        int[] fibo = new int[n];
        fibo[0] = 1;
        fibo[1] = 2;
        for (int i = 2; i < n; i++) {
            fibo[i] = fibo[i - 1] + fibo[i - 2];
        }
        return fibo;
    }

    static int euklides(int a, int b) {
        while (a != b) {
            if (a > b)
                a -= b;
            else
                b -= a;
        }
        return a;
    }

    static void multiplicationTable(int n){
        int[][] multiTable = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                multiTable[i][j] = (i+1)*(j+1);
            }
        }
        for (int[] a: multiTable) {
            System.out.println(Arrays.toString(a));
        }
    }

    static int sumNumbersFromFile(String filename) throws IOException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
        int sum = 0;
        while (sc.hasNextLine()){
            sum += Integer.valueOf(sc.nextLine());
        }
        sc.close();
        return sum;
    }

}
