package everyDayTask.javaExam.randomNumbersArray;

import java.lang.reflect.Array;
import java.util.*;

public class RandomArrayGenerator {

    public static void main(String[] args) {
        int arr[][] = generateArray();
        for (int[] a : arr) {
            System.out.println(Arrays.toString(a));

        }
        int[][] smalArr = {{1,1,-1,1},{1,1,1,1},{1,1,1,1},{1,1,1,1}};
        System.out.println((countRowsWithValueLessThanMultiplicationOfRowAndCol(arr)));
    }

    private static int[][] generateArray() {
        Random random = new Random();
        int arrSize = 10 + random.nextInt(11);
        int[][] arr = new int[arrSize][arrSize];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                int numbers = -100 + random.nextInt(201);
                arr[i][j] = numbers;
            }
            arr[i][i] = generateRandValue();
        }
        return arr;

    }

    private static int generateRandValue() {
        Random random = new Random();
        int randValue = random.nextInt(10);
        int value = 0;
        if (randValue % 2 == 0) {
            value = -1;
        } else {
            value = 1;
        }
        return value;
    }

    private static int[] showMaxInt(int[][] multiArr) {
        int[] maxNumbers = new int[2];
        int max = 0;
        int max2 = 0;
        for (int i = 0; i < multiArr.length; i++) {
            for (int j = 0; j < multiArr.length; j++) {
                if (max < multiArr[i][j]) {
                    max2 = max;
                    max = multiArr[i][j];
                } else if (multiArr[i][j] != max && multiArr[i][j] > max2) {
                    max2 = multiArr[i][j];
                }
            }
        }
        maxNumbers[0] = max;
        maxNumbers[1] = max2;
        return maxNumbers;
    }

    static int sumOfNotEvenColAndEvenRows(int[][] multiArr){
        int sum = 0;
        for (int i = 1; i < multiArr.length; i += 2) {
            for (int j = 0; j < multiArr.length; j += 2) {
                sum += multiArr[i][j];
            }
        }
        return sum;
    }

    static int countRowsWithValueLessThanMultiplicationOfRowAndCol(int[][] multiArr){
        int count = 0;
        for (int i = 0; i < multiArr.length; i++) {
            for (int j = 0; j < multiArr[i].length; j++ ) {
                int multi = i * j;
                if(multi > multiArr[i][j]){
                    count++;
                }
            }
        }
        return count;
    }
}
