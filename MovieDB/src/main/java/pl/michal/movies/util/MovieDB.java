package pl.michal.movies.util;

import pl.michal.movies.crud.SimpleCRUD;
import pl.michal.movies.entity.Movie;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Lenovo on 13.07.2017.
 */
public class MovieDB {
    public void start(){
        showMenu();
        SimpleCRUD<Movie> crud = new SimpleCRUD(Movie.class);
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            if (sc.nextLine().equals("1")) {
                Movie movie = new Movie();
                System.out.println("Podaj tytuł filmu:");
                movie.setTitle(sc.nextLine());
                System.out.println("Podaj rok premiery: ");
                movie.setYear(Integer.parseInt(sc.nextLine()));
                System.out.println("Podaj czas trwania w godzinach:");
                movie.setDuration(Double.parseDouble(sc.nextLine()));
//                System.out.println("Podaj gatunek filmu: ");
//                Genre genre = new Genre(sc.nextLine());
//                movie.setGenre(genre);
                System.out.println("Napisz o czym jest film");
                movie.setDescription(sc.nextLine());
                crud.insertOrUpdate(movie);
                showMenu();
            } else if (sc.nextLine().equals("2")) {
                Movie movie = new Movie();
                System.out.println("Podaj tytuł filmu:");
                movie.setTitle(sc.nextLine());
                System.out.println("Podaj rok premiery: ");
                movie.setYear(Integer.parseInt(sc.nextLine()));
                System.out.println("Podaj czas trwania w godzinach:");
                movie.setDuration(Double.parseDouble(sc.nextLine()));
                System.out.println("Napisz o czym jest film");
                movie.setDescription(sc.nextLine());
                crud.delete(movie);
                showMenu();
            } else if (sc.nextLine().equals("3")) {
                System.out.println("Podaj id filmu: ");
                int id = Integer.parseInt(sc.nextLine());
                Movie movie = crud.select(id);
                System.out.println("Tytuł: " + movie.getTitle()
                        + "Rok premiery: " + movie.getYear()
                        + "Gatunek: " + movie.getGenre().getGenre()
                        + "Czas trwania:  " + movie.getDuration()
                        + "Opis: " + movie.getDescription());
                showMenu();
            } else if (sc.nextLine().equals("4")) {
                List<Movie> movies = crud.select();
                for (Movie m : movies) {
                    System.out.println("Id: " + m.getId() + "\n" + "Tytuł: " + m.getTitle());
                }
                showMenu();
            } else if (sc.nextLine().equals("5")) {
                System.exit(0);
            }

        }
    }


    private static void showMenu(){
        System.out.println("1. Zapisz film do bazy" + "\n"
                + "2. Skasuj film" + "\n"
                + "3. Wybierz film" + "\n"
                + "4. Wypisz wszystkie filmy" + "\n"
                + "5. Wyjdź");
    }

}
