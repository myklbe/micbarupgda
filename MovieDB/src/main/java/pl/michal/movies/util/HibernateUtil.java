package pl.michal.movies.util;

import com.fasterxml.classmate.AnnotationConfiguration;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Lenovo on 13.07.2017.
 */
public class HibernateUtil {
    private static final SessionFactory sf = new Configuration().configure().buildSessionFactory();

    private HibernateUtil(){}

    public static Session getSession(){
        return sf.openSession();
    }



}
