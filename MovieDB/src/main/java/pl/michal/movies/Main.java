package pl.michal.movies;

import javafx.beans.property.SimpleListProperty;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.michal.movies.crud.CRUD;
import pl.michal.movies.crud.SimpleCRUD;
import pl.michal.movies.entity.Genre;
import pl.michal.movies.entity.Movie;
import pl.michal.movies.util.HibernateUtil;
import pl.michal.movies.util.MovieDB;

import javax.persistence.metamodel.EntityType;

import java.awt.geom.GeneralPath;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Lenovo on 13.07.2017.
 */
public class Main {


    public static void main(String[] args) {
//        MovieDB movie = new MovieDB();
//        movie.start();


        Movie m = new Movie("Film", 2009, 2.3, "jakis tam film");
        SimpleCRUD<Movie> crud = new SimpleCRUD<Movie>(Movie.class);

        crud.insertOrUpdate(m);

        System.out.println(crud.select(1).getTitle());

    }
}