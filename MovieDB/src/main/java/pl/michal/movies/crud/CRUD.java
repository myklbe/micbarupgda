package pl.michal.movies.crud;

import java.util.List;

/**
 * Created by Lenovo on 13.07.2017.
 */
public interface CRUD<T> {
    public void insertOrUpdate(T obj);
    public void delete(T obj);
    public T select(int id);
    public List<T> select();

}
