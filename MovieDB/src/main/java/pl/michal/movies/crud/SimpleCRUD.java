package pl.michal.movies.crud;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.michal.movies.entity.Movie;
import pl.michal.movies.util.HibernateUtil;

import javax.management.Query;
import java.util.List;

/**
 * Created by Lenovo on 13.07.2017.
 */
public class SimpleCRUD<T> implements CRUD<T> {
    private Class<T> clazz;

    public SimpleCRUD(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void insertOrUpdate(T obj) {
        Session session = HibernateUtil.getSession();
        Transaction t = session.beginTransaction();
        session.saveOrUpdate(obj);
        t.commit();
        session.flush();
        session.close();
    }

    public void delete(T obj) {
        Session session = HibernateUtil.getSession();
        session.delete(obj);
        session.close();
    }

    public T select(int id) {
        Session session = HibernateUtil.getSession();
        T obj = (T) session.get(clazz, id);
        session.close();
        return obj;
    }

    public List<T> select() {
        Session session = HibernateUtil.getSession();
        List<T> list = session.createQuery("from " + clazz.getSimpleName()).list();
        session.close();
        return list;
    }
}
