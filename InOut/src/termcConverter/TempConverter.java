package termcConverter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {
	private final String path = "resources/";

	public double toKelvin(double temp) {
		if (temp < -273.15) {
			System.out.println("To jest fizycznie niemożliwe, nigdzie nie jest tak cholernie zimno");
		} else {
			return temp + 273.15;
		}
		return 0;
	}

	public double toFahrenheit(double temp) {
		return (1.8 * temp) + 32;
	}

	public double[] readTemp(String filename) {
		double[] ret = new double[countLines(filename)];
		File f = new File(path + filename);

		try {
			Scanner sc = new Scanner(f);
			String currentline;
			int i = 0;
			while (sc.hasNextLine()) {
				currentline = sc.nextLine();
				ret[i++] = Double.parseDouble(currentline.replace(",", "."));
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		return ret;
	}

	private int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
	
	public void writeTemp(double[] temp){
		try{
			FileOutputStream fosK = new FileOutputStream(path + "tempK.txt");
			FileOutputStream fosF = new FileOutputStream(path + "tempF.txt");
			
			PrintWriter pwK = new PrintWriter(fosK);
			PrintWriter pwF = new PrintWriter(fosF);
			
			for (double c: temp){
				pwK.println(toKelvin(c));
				pwF.println(toFahrenheit(c));
			}
			
			pwK.close();
			pwF.close();
		} catch(FileNotFoundException e){
			System.out.println(e.getMessage());
		}
	}

}
