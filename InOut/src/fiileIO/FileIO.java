package fiileIO;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileIO {
    private String filename;

    public FileIO(String filename) {
        this.filename = filename;
    }

    public String[] readFile(){
     File f = new File(filename);
        try (Scanner sc = new Scanner(f)) {
         while (sc.hasNext()){
             String[] rf = sc.nextLine();
             return rf;
         }
     } catch (FileNotFoundException e) {
         e.printStackTrace();
     }return null;

 }
    public String readFileAsString(){
     String rf = readFile().toString();
     return rf;
}
//+readLine(line:int):String
//+readLines(lineStart:int, lineEnd:int):String[]
//+writeLine(line:int, content:String):void
//+writeLines(lineStart:int, content:String[]):void
//+updateLine(line:int, content:String):void
//+updateLines(lineStart:int, content:String[]):void
//+append(content:String):void
//+insertBefore(line:int, content:String):void
//+insertAfter(line:int, content:String):void
//+copyFile(newFilename:String):void
}
