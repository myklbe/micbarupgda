package averageChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {
	private String filename;

	public AvgChecker(String filename) {
		this.filename = filename;
	}

	public void process() {
		File f = new File(filename);
		double avg = 0;
		LinkedList<String> fileContent = new LinkedList<>();
		try (Scanner sc = new Scanner(f)) {
			String currenline;
			while (sc.hasNextLine()) {
				currenline = sc.nextLine();
				if(!currenline.equals("")){
					fileContent.add(currenline);
					avg += getAverage(currenline);
				}
			} 
			avg /= fileContent.size();
			sc.close();
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pwr = new PrintWriter(fos);
			
			for(String line: fileContent){
				if (getAverage(line) > avg){
					pwr.println(line);
				}
			} pwr.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} 
	}

	private double getAverage(String line){
		String[] temp = line.split("\t");
		double sum = 0;
		for(int i = 1; i < temp.length; i++){
			sum += Double.parseDouble(temp[i]);
		}
		return sum / (temp.length - 1);
	}

}
// odczytaj warto�ci z filename
// policz �redni� dla currentline
// oblicz �redni� wszystkich �rednich
// por�wnaj ze �redni� wszystkich �rednich
// nadpisz plik pozostawiaj�c tylko te powy�ej �redniej �rednich