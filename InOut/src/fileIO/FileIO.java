package fileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileIO {
	private String filename;

	public FileIO(String filename) {
		super();
		this.filename = filename;
	}

	public String[] readFile() {
		List<String> fileList = new ArrayList<String>();
		File f = new File(filename);
		try (Scanner sc = new Scanner(f)) {
			while (sc.hasNextLine()) {
				fileList.add(sc.nextLine());
			}
			String[] fileArray = new String[fileList.size()];
			fileList.toArray(fileArray);
			return fileArray;
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	public String readFileAsString() {
		String[] str = readFile();
		String file = "";
		for(String l : str){
			file += l + "\t";
		}
		return file;
	}

	public String readLine(int line) {
		String[] str = readFile();
		return str[line].toString();
	}

	public String[] readLines(int lineStart, int lineEnd) {
		String[] lines = readFile();
		List<String> rl = new LinkedList<>();
		if (lineStart > 0 && lineEnd >= lineStart){
			for (int i = lineStart; i <= lineEnd + 1;) 
			 {
				rl.add(lines[i++]);
				rl.toArray(lines);
				return lines;
			}
		
		}
		return null;
	}
	public void readLines(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj pierwsz� linijk�: ");
		int lineStart = sc.nextInt();
		System.out.println("Podaj drug� linijk�: ");
		int lineEnd = sc.nextInt();
		String[] lines = readFile();
		List<String> rl = new LinkedList<>();
		for (int i = lineStart; i <= lineEnd + 1; i++) {
			if (lineStart > 0 && lineEnd >= lineStart) {
				rl.add(lines[i]);
				rl.toArray(lines);
				System.out.println(lines);
			}
	} 
	}
	public void writeLines(int line, String content){
		File f = new File(filename);
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pwr = new PrintWriter(fos);
			pwr.println(content);
			
			
			Scanner sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String toString() {
		return "FileIO [filename=" + filename + ", readFile()=" + Arrays.toString(readFile()) + ", readFileAsString()="
				+ readFileAsString() + "]";
	}
	
	

}
