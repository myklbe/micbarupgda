package columner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {
	private final String path = "resources/";
	private String filename;
	private double[] currentColumn;
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner(String filename) {
		this.filename = filename;
	}

	public Columner() {

	}

	private void readFile() {
		if (fileContent.size() == 0) {
			File f = new File(path + filename);
			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void readColumn(int column) {
		readFile();
		int maxColNum = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColNum) {
			currentColumn = new double[fileContent.size()];
			int i = 0;
			for (String line : fileContent) {
				String[] cols = line.split("\t");
				currentColumn[i++] = Double.parseDouble(cols[column - 1].replace(",", "."));
			}
		}
		
	}

	@Override
	public String toString() {
		return "Columner [currentColumn=" + Arrays.toString(currentColumn) + ", fileContent=" + fileContent + "]";
	}

	public double sumColumn(int column) {
		double sum = 0;
		readColumn(column);
		for (double r : currentColumn) {
			sum += r;
		}
		return sum;
	}

	public double avgColumn(int column) {
		readColumn(column);
		return sumColumn(column) / currentColumn.length;
	}

	public int countColumn(int column) {
		readColumn(column);
		return currentColumn.length;
	}

	public double maxColumn(int column){
		double max = 0;
		readColumn(column);
		for(double m: currentColumn){
			if (m > max){
				max = m;
			}
		} 
		return max;
	}
	public double minColumn(int column){
		double min = maxColumn(column);
		readColumn(column);
		for(double m: currentColumn){
			if (m < min){
				min = m;
			}
		} 
		return min;
	}
	public void writeColumn(String filename){
		try {
			FileOutputStream fos = new FileOutputStream(path + filename);
			PrintWriter pw = new PrintWriter(fos);
			try {
				for (double t: currentColumn){
					pw.println(t);
				}
				pw.close();
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
			
	}
	

}