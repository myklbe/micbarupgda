package presentChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PresentChecker {
	private String filename;

	public PresentChecker() {

	}

	public PresentChecker(String filename) {
		this.filename = filename;
	}

	private boolean checkIfExists(String sentence) {
		File f = new File(filename);
		try (Scanner sc = new Scanner(f)) {
			String currentline;
			while (sc.hasNextLine()) {
				currentline = sc.nextLine();
				String[] st = currentline.split("\t");
				for (String s : st) {
					if (sentence.equals(s)) {
						return true;
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public void readWords() {
		File f = new File(filename);
		Scanner sc = new Scanner(System.in);
		System.out.println("Sprawdz czy wystepuje slowo: ");
		String sentence = sc.nextLine();
		if (checkIfExists(sentence)) {
			System.out.println("S�owo istnieje w " + filename);
		} else {
			System.out.println("S�owo nie istnieje w " + filename);
		}

	}

}
