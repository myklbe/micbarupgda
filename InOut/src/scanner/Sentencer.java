package scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Sentencer extends Sentence {

	public void writeSentence(String filename, String sentence){
		File f = new File("resources/" +filename);
		
		try {
			FileOutputStream fos = new FileOutputStream(f, true); // drugi parametr "true" ustawia metod� na appendowanie, bez niego plik jest nadpisywany
			PrintWriter pw = new PrintWriter(fos);
			pw.println(sentence);
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		
		
	}
}
