package scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	private final String filename = "resources/currency.txt";
	
	private double readCourse (String currency){
		File f = new File(filename);
		currency = currency.toUpperCase();
		
		try {
			String currentline = "";
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()){
				currentline = sc.nextLine();
				String[] temp = currentline.split("\t");
				String[] values = temp[0].split(" ");
				if(values[1].equalsIgnoreCase(currency)){
					return Double.parseDouble(temp[1].replace(",",".")) / Integer.parseInt(values[0]);
				}
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("file not found");
		}
		
		return 0.0;
	}
	
	public double convert(double money, String toCurrency){
		return money / readCourse(toCurrency);
	}
	public double convert(double money, String to, String from){
		return money * readCourse(to) / readCourse(from);
	}
	
	
}
