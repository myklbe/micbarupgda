package lengthChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
	private boolean isProperLength(String arg, int len) {
		return arg.length() > len;
	}

	private String[] readFile(String filename) {
		File f = new File(filename);
		String[] words = new String[countLines(filename)];

		try {
			Scanner sc = new Scanner(f);
			int i = 0;
			while (sc.hasNextLine()) {
				words[i++] = sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		return words;
	}

	private int countLines(String filename) {
		File f = new File(filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}

	private void writeFile(String[] fileContent, int len) {
		try {
			FileOutputStream words = new FileOutputStream("resources/words_" +len +".txt");
			PrintWriter pWords = new PrintWriter(words);
			
			for(String f: fileContent){
				if (isProperLength(f, len) == true){
					pWords.println(f);
				}
			} 
			pWords.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	public void make(String fileInput, int len){
		writeFile(readFile(fileInput), len);
	}

}
