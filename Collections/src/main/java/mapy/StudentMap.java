package mapy;

import java.util.HashMap;
import java.util.Map;

public class StudentMap {

	public static void main(String[] args) {

		Student marek = new Student(007, "Marek", "Kowalski");
		Student kasia = new Student(100200, "Kasia", "Dobrowolska");
		Student gerwazy = new Student(100400, "Gerwazy", "Potar�o");

		Map<Integer, Student> students = new HashMap<>();
		students.put(007, marek);
		students.put(100100, kasia);
		students.put(100400, gerwazy);

		students.put(100300, new Student(100300, "Donald", "Tusk"));

		// System.out.println(studens.get(100200));

		System.out.println(students.get(100200) != null);

		System.out.println(students.size());

		for (Integer s : students.keySet()) {
			System.out.println(students.get(s));
		}

	}

}
