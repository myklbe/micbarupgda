package mapy;

import java.util.HashMap;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();

	public void addStudent(int indexID, String name, String surname) {
		Student student = new Student(indexID, name, surname);
		students.put(student.getNumerIndeksu(), student);
	}

	public boolean studentExists(int indexID) {
		// delegacja
		return students.containsKey(indexID);
	}

	public Student getStudent(int indexID) {
		// delegacja
		return students.get(indexID);

	}

	public int studentNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getNumerIndeksu() + ":" + s.getImie() + " " + s.getNazwisko());
		}
	}

}
