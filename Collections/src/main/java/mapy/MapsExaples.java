package mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExaples {

	public static void main(String[] args) {
		
		
		Map<String, String> dictionary = new HashMap<>();
		dictionary.put("kot", "cat");
		dictionary.put("pies", "dog");
		
		System.out.println("Klucze: ");
		for (String key: dictionary.keySet()) {
			System.out.println(key);
		}
		
		System.out.println("Warto�ci: ");
		for (String value: dictionary.values()) {
			System.out.println(value);
		}
		
		System.out.println("Pary klucz:wartosc : ");
		for(Map.Entry<String, String> pair : dictionary.entrySet()){
			System.out.println(pair.getKey() +" : " +pair.getValue());
		}
		dictionary.remove("pies");
		System.out.println("Pary klucz:wartosc po usunu�ciu psa : ");
		for(Map.Entry<String, String> pair : dictionary.entrySet()){
			System.out.println(pair.getKey() +" : " +pair.getValue());
		}
		System.out.println(dictionary.get("Pies"));
		System.out.println(dictionary.getOrDefault("Pies", "No translation"));
		
		
	}
	
	
}
