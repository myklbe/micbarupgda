package mapy;

import java.util.TreeMap;

public class UniversityExmple {

	public static void main(String[] args) {

		University university = new University();

		university.addStudent(100100, "Kamil", "Ordenko");
		university.addStudent(100200, "Dariusz", "S�onimski");
		university.addStudent(100300, "Marta", "Zorkacz");
		university.addStudent(100400, "Kasia", "Gala");
		university.addStudent(100500, "Wac�aw", "Nieporamski");

		System.out.println(university.studentExists(100200));

		System.out.println(university.getStudent(100400).getImie());

		System.out.println(university.studentNumber());

		university.showAll();

	}

}
