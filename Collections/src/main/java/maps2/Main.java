package maps2;

/**
 * Created by Lenovo on 29.06.2017.
 */
public class Main {
    public static void main(String[] args) {

        Workplace place = new Workplace();
        place.addWorker(1,"marian_kowalski");
        place.addWorker(2,"Jacek_Modrzew");
        place.addWorker(3,"Karol_Magik");
        place.addWorker(4,"Zygfryd_Pierwszy");

        place.printAllOcupants();
        place.printAllRooms();

        System.out.println(place.getOcupant(1));
        System.out.println(place.getRoom("Zygfryd_Pierwszy"));
    }



}
