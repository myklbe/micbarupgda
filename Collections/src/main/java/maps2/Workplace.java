package maps2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Lenovo on 29.06.2017.
 */
public class Workplace {
    private HashMap<Integer, String> map = new HashMap();
    private HashMap<String, Integer> reverseMap = new HashMap();


    public void addWorker(int rooNumber, String name_surname){
        map.put(rooNumber,name_surname);
        reverseMap.put(name_surname,rooNumber);
    }
    public String getOcupant(int roomNuber){
        return map.get(roomNuber);
    }
    public int getRoom(String ocupant){
        return reverseMap.get(ocupant);
    }

    public void printAllOcupants(){
        for (Object i: map.values()) {
            System.out.println(i);
        }
    }
    public void printAllRooms(){
        for (Object i: map.keySet()){
            System.out.println(i);
        }
    }

}
