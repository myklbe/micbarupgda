package listofTen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.RuntimeErrorException;

public class ListofTen {

	public static void main(String[] args) {

		int[] array = { 1, 2, 4, 2, 5, 12, 3, 2, 5, 6 };
		List<Integer> list = Arrays.asList(4, 2, 2, 1, 5, 29, 3, 8);

		System.out.println(countDuplicates(array, list));
		
	}

	public static int countDuplicates(int[] array, List<Integer> list) {
		int count = 0;

		if (array.length != list.size()){
			throw new RuntimeErrorException(null, "Collection of element is not the same size");
		}
		
		int elements = Math.min(array.length, list.size());
		for (int i = 0; i < elements; i++) {
			if (array[i] == list.get(i)) {
				count++;
			}
		}
		return count;

	}

}
