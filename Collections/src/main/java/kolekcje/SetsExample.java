package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {

		int[] tab = { 1, 2, 3, 4, 5, 6, 7, 8, 8, 6, 5, 4, 4 };

		
		// Set<Integer> set = new HashSet<>();
		Set<Integer> set = new TreeSet<>();
		set.add(tab[0]);

		set.addAll(Arrays.asList(40, 4, 7, 4, 5, 6, 7, 8, 8, 6, 5, 4, 4));

		
		
		System.out.println(set.size());

		for (int i : set) {
			System.out.println(i);
		}

		set.remove(2);
		set.remove(3);

		for (int i : set) {
			System.out.println(i);
		}
		
		Iterator<Integer> iterator = set.iterator();
		System.out.println("Jeden element : "+set.iterator().next());
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
		
		
	}

}
