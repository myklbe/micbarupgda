package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListArrayDeclarations {

	public static void main(String[] args) {
		
		
		
		List<Integer> lista = Arrays.asList(1,2,3,4);    // taka tablica jest nieedytowalna
		// mo�na uczynic ja edytowaln�
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(1,2,3));
 		
		
		int [] tab1 = {1,2,3,4};
		int [] tab2 = null;
		
		// je�li tablica jest ju� zainicjowana (ju� istnieje) to trzeba tak:
		tab2 = new int[] {1,2,3,4};
		//  albo tak
		tab2 = new int[2];
		tab2[0] = 1;
		tab2[1] = 2;
		
		int [][] tab2dim1 = {{1,2},{3},{2,3,5}};
		// tablica tablic zer
		
		
	}

}
