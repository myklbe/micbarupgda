package kolekcje;

import java.util.List;

public class ListMath {

	
	
	public static int sum(List <Integer> numbers){
		int suma = 0;
		for(int i : numbers){
			suma += i;
		}
		return suma;
	}
	
	public static int multi(List <Integer> numbers){
		int multi = 1;
		for(int i : numbers){
			multi *= i;
		}
		return multi;
	}
	public static double average(List <Integer> numbers){
		return (double)sum(numbers)/numbers.size();
	}
	
	
	
}
