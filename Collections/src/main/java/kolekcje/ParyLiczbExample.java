package kolekcje;

import java.util.HashSet;
import java.util.Set;

public class ParyLiczbExample {

	public static void main(String[] args) {
		
		Set<ParyLiczb> pairs = new HashSet<>();
		
		pairs.add(new ParyLiczb(1, 2));
		pairs.add(new ParyLiczb(2, 1));
		pairs.add(new ParyLiczb(1, 1));
		pairs.add(new ParyLiczb(1, 2));
		
		for(ParyLiczb paryliczb : pairs){
			System.out.println(paryliczb);
		}
		
		
	}

}
