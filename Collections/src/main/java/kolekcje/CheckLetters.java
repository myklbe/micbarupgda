package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckLetters {
	String text;

	public CheckLetters(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	// stw�rz zbi�r oparty o tablice znak�w stworzon� ze string
	// porownaj d�ugo�� tablicy ze zbiorem, jest jest taki sam to elementy si�
	// nie powtarzaja

	public static boolean containDuplicates(String text) {
		Set<Character> set = new HashSet<>();
		for (char i : text.toCharArray()) {
			set.add(i);
		}return text.toCharArray().length == set.size();
		
		// to samo:
		/*if (t.length == set.size()) {
			return false;
		} else {
			return true;
		}*/
	}

}
