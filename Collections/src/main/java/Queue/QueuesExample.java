package Queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueuesExample {

	public static void main(String[] args) {
		
		Queue<Integer> queue = new PriorityQueue<>();
		queue.add(1);
		queue.add(13);
		queue.add(17);
		queue.add(21);
		queue.add(41);
		
		while (!queue.isEmpty()){
			System.out.println(queue.poll());
		}
		
		
	}
	
	
	
	
	
}
