package Queue;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {

		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>() { // klasa
																				// anonimowa, isnieje w �rodku kodu 
																				// i nie posiada nazwy
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}

		});

		persons.offer(new Person("Michael", 25));
		persons.offer(new Person("Steve", 67));
		persons.offer(new Person("Carl", 34));
		persons.offer(new Person("Iris", 25));
		persons.offer(new Person("Jessie", 13));

		while (!persons.isEmpty()) {
			System.out.println(persons.poll());
		}

	}

}
