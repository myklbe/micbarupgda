package Queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortowanieListy {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();

		persons.add(new Person("Michael", 25));
		persons.add(new Person("Steve", 67));
		persons.add(new Person("Carl", 34));
		persons.add(new Person("Iris", 25));
		persons.add(new Person("Jessie", 13));

		System.out.println("Przed sortowaniem: ");
		for (Person person : persons) {
			System.out.println(person);
		}

		Collections.sort(persons);

		System.out.println("Po sortowaniu: ");
		for (Person person : persons) {
			System.out.println(person);
		}

		Collections.shuffle(persons);
		System.out.println("Po tasowaniu: ");
		for (Person person : persons) {
			System.out.println(person);
		}

	}

}
