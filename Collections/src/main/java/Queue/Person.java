package Queue;

public class Person implements Comparable<Person> {
	private final String name;
	private final int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Person o) {
		return this.age - o.age; // �eby ustawi� kolejno��, je�li return < 0
									// b�dzie sortowa� od najmniejszej, mo�na
									// te� elseif
	}
}
