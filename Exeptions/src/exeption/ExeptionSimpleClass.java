package exeption;

public class ExeptionSimpleClass {
	public void make(int a) throws Exception, IllegalArgumentException{
		if (a == 55){
			throw new IllegalArgumentException("Niepoprawny argument");
		}
		if (a == 5){
			throw new Exception("a jest r�wne 5!");
		} else {
			System.out.println("a nie jest r�wne 5!");
		}
	}
	
	public void exceptionExample(String name) throws PawelException{
		if (name.equalsIgnoreCase("pawel")){
			throw new PawelException("Jeste� Paw�em");
		}
	}
}
