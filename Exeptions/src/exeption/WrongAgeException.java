package exeption;

public class WrongAgeException extends Exception {
	
	public WrongAgeException(String message) {
		super(message);
	}
	private WrongAgeException(){
		super();
	}
}
