package exeption;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadranticEquation {
	private int a;
	private int b;
	private int c;

	public QuadranticEquation(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public QuadranticEquation() {
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	private int getNumber() {
		int in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Put a whole number: ");
		try {
			in = sc.nextInt();
		} catch (InputMismatchException e){
			in = 0;
		}
		return in;
	}

	public double[] solve() throws ArithmeticException {
		while (a == 0 && b == 0 && c == 0){
		a = getNumber();
		b = getNumber();
		c = getNumber();
		}
		
		double x1, x2, delta;
		delta = (b * b) - (4 * a) * c;
		if (delta >= 0) {
			x1 = (-Math.sqrt(delta) - b)/(2*a);
			x2 = (Math.sqrt(delta) - b)/(2*a);
		} else { 
			throw new ArithmeticException("Delta lower than 0");
		}
		
		return new double[] {x1,x2};
	}

	// storzona instancja z parametrami ma policzy� r�wnanie kwadratowe na
	// podstawie podanych parametr�w i umieszcza�a wyniki w dwuelementowej
	// tablicy, je�li instancja b�dzie bez parametr�w
	// trzeba je pora� z konsoli i urzy� we wzorze

}
