package exeption;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers extends Exception {

	public double readDouble() {
		double in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb�: ");
		try {
			in = sc.nextDouble();
		} catch (InputMismatchException e) {
			in = 0;
		}
		return in;
	}

	public String readString() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj �a�cuch znak�w: ");
		return sc.nextLine();
	}

	public double readInt() {
		int in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb�: ");
		try {
			in = sc.nextInt();
		} catch (InputMismatchException e) {
			in = 0;
		}
		return in;
	}
}

// ka�da z metod musi posiada� scanner i return wpisan� warto�c, trzeba
// sprawdzi� czy typ jest zgodny ze zwracanym, je�li nie to Input
// MismatchException