package exeption;

public class Square {

	public static double square(int n) throws IllegalArgumentException {
		if (n < 0) {
			throw new IllegalArgumentException("number is less than 0");
		} return Math.sqrt(n);
	}

}
