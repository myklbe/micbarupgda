package queue;

import sun.invoke.empty.Empty;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * Created by Lenovo on 27.06.2017.
 */
public class Queue implements SimpleQueue {

    private int[] data = new int[8];
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if (end == end && isEmpty) {
            int[] newData = new int[data.length];
            if (start == end) {
                int i = 0;
                while (!isEmpty) {
                    newData[i] = poll();
                    i++;
                }
                data = newData;
                start = 0;
                end = i;
            }
            data[end] = value;
            end = (end + 1) % data.length;
            isEmpty = false;
        }
    }

    @Override
    public int poll() {
        if (!isEmpty) {
            int p = data[start];
            start = (start + 1) % data.length;
            return p;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int peek() {
        if (!isEmpty) {
            return data[start];
        } else {
            throw new NoSuchElementException();

        }


    }
}