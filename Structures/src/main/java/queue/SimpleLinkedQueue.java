package queue;

import lists.SimpleLinkedList;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Created by Lenovo on 27.06.2017.
 */
public class SimpleLinkedQueue implements SimpleQueue {
    Element first;
    Element last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int value) {
        Element element = new Element(value);
        if (isEmpty()) {
            first = element;
        } else {
            last.next = element;
        }
        last = element;
    }

    @Override
    public int poll() {
        int result = peek();

        first = first.next;
        if (first == null) {
            last = null;
        }

        return result;
    }

    @Override
    public int peek() {
        if (!isEmpty()) {
            return first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    private static class Element {
        int value;
        Element next;

        public Element(int value) {
            this.value = value;
        }
    }

}
