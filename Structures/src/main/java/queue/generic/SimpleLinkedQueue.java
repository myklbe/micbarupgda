package queue.generic;

import java.util.NoSuchElementException;

/**
 * Created by Lenovo on 27.06.2017.
 */
public class SimpleLinkedQueue<T> implements SimpleQueue<T> {
    Element<T> first;
    Element<T> last;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(T value) {
        Element element = new Element(value);
        if (isEmpty()) {
            first = element;
        } else {
            last.next = element;
        }
        last = element;
    }

    @Override
    public T poll() {
        T result = peek();

        first = first.next;
        if (first == null) {
            last = null;
        }

        return result;
    }

    @Override
    public T peek() {
        if (!isEmpty()) {
            return first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    private static class Element<T> {
        T value;
        Element<T> next;

        public Element(T value) {
            this.value = value;
        }
    }

}
