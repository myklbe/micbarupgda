package queue.generic;

/**
 * Created by Lenovo on 27.06.2017.
 */
public interface SimpleQueue<T> {

    public boolean isEmpty();

    public void offer(T value);

    public T poll();

    public T peek();

}
