package queue.generic;

import lists.SimpleArrayList;

/**
 * Created by Lenovo on 27.06.2017.
 */
public class Main {
    public static void main(String[] args) {

        SimpleQueue<String> queue = new Queue<>();
        queue.offer("AA");
        queue.offer("BB");
        queue.offer("cc");

        System.out.println(queue.peek());

        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }

    }


}
