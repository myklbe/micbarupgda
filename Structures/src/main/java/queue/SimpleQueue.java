package queue;

/**
 * Created by Lenovo on 27.06.2017.
 */
public interface SimpleQueue {

    public boolean isEmpty();

    public void offer(int value);

    public int poll();

    public int peek();

}
