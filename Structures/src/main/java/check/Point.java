package check;

/**
 * Created by Lenovo on 27.06.2017.
 */
public class Point {
    int x;
    int y;

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
