package lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Adrian on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

//        SimpleList list = new SimpleArrayList();
//
//        list.add(1);
//        list.add(2);
//        list.add(5);
//
//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//        System.out.println(list.get(2));
//

//        int[] data = {0,0,2,1,4,5,0};
//        data = Arrays.copyOf(data, (2*data.length));
//        System.out.println(Arrays.toString(data));
//
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(1);
        a.add(2);
        a.add(2);
        a.add(3);
        a.add(3);
        HashSet<Integer> set = new HashSet<>();
        set.addAll(a);
        a.removeAll(a);
        a.addAll(set);
        System.out.println(a.toString());



    }
}
