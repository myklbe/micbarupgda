package pl.michal.repository;


import org.springframework.data.repository.CrudRepository;
import pl.michal.entity.Staff;

public interface StaffRepository extends CrudRepository<Staff, Long> {
}
