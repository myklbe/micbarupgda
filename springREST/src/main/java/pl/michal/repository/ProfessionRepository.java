package pl.michal.repository;

import org.springframework.data.repository.CrudRepository;
import pl.michal.entity.Profession;


public interface ProfessionRepository extends CrudRepository<Profession, Long> {
}
