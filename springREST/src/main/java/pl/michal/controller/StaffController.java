package pl.michal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.michal.entity.Profession;
import pl.michal.entity.Staff;
import pl.michal.repository.ProfessionRepository;
import pl.michal.repository.StaffRepository;


import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/staff")
public class StaffController {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/add")
    public Staff add(@RequestParam(name = "name") String name,
                     @RequestParam(name = "lastname") String lastname,
                     @RequestParam(name = "salary") String salary,
                     @RequestParam(name = "profession") String profession){
        Long professionId = Long.valueOf(profession);
        Profession prof = professionRepository.findOne(professionId);
        Staff staff = new Staff();
        staff.setName(name);
        staff.setLastname(lastname);
        double sal = Double.valueOf(salary);
        staff.setSalary(sal);
        staff.setProfession(prof);
        staffRepository.save(staff);
        return staff;
    }
    @RequestMapping("/show")
    public List<Staff> show(){
        return (List<Staff>) staffRepository.findAll();
    }
    @RequestMapping("/show{id}")
    public Staff showByID(@PathVariable(name = "id") String id){
        return staffRepository.findOne(Long.valueOf(id));
    }

}
