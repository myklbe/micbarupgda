package pl.michal.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.michal.entity.Profession;
import pl.michal.entity.Staff;
import pl.michal.repository.ProfessionRepository;
import pl.michal.repository.StaffRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/profession")
public class ProfessionController {
        @Autowired
        private StaffRepository staffRepository;
        @Autowired
        private ProfessionRepository professionRepository;

        @RequestMapping("/add")
        public Profession add(@RequestParam(name = "name") String name){
            Profession prof = new Profession(name);
            professionRepository.save(prof);
            return prof;
        }
        @RequestMapping("/show")
        public List<Profession> show(){
            return (List<Profession>) professionRepository.findAll();
        }

        @RequestMapping("/show{id}")
        public Profession showByID(@PathVariable(name = "id") String id){
            return professionRepository.findOne(Long.valueOf(id));
        }
}
