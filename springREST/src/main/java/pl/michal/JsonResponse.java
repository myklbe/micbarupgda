package pl.michal;

/**
 * Created by Lenovo on 25.07.2017.
 */
public class JsonResponse {
    private String message;

    public JsonResponse(){}

    public JsonResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
