package StringTest;

public class StringUtil {
	private String str;

	public StringUtil(String str) {
		this.str = str;
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil append(String arg) {
		str = str + arg;
		return this;
	}

	public StringUtil letterSpacing() {

		String[] space = str.split("");
		str = "";
		for (String s : space) {
			str += s + " ";

		}
		str = str.substring(0, str.length() - 1);
		return this;
	}

	public StringUtil reverse() {
		String str2 = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			str2 += str.charAt(i);
		}
		str = str2;
		return this;
	}

	public StringUtil getAlhpabet() {
		for (char c = 97; c <= 122; c++) {
			str += c;
		}
		return this;
	}

	public StringUtil getFirstLetter() {
		String[] s = str.split("");
		str = "" + s[0];
		return this;
	}

	public StringUtil limit(int n) {
		str = str.substring(n);
		return this;
	}

	public StringUtil insertAt(String string, int n) {
		str = str.substring(0, n) + string + str.substring(n);

		return this;
	}

	public StringUtil resetText() {
		str = "";
		return this;
	}

	public StringUtil swapLetters() {
		str = str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0) + "";
		return this;
	}

	public StringUtil createSentence() {
		String first = "" + str.charAt(0);
		String last = "" + str.charAt(str.length() - 1);
		if (last == ".") {
			str = first.toUpperCase() + str.substring(1);
		} else {
			str = first.toUpperCase() + str.substring(1) + ".";
		}
		return this;
	}

	public StringUtil cut(int from, int to) {
		String ret = "";
		for (int i = from; i < to; i++) {
			ret += str.charAt(i);
		}
		str = ret;
		return this;
	}
	public StringUtil pokemon() {
		String newString = "";
		for(int i = 0; i < str.length(); i++) {
			if(i % 2 == 0) {
				newString += (str.charAt(i) + "").toLowerCase();
			} else {
				newString += (str.charAt(i) + "").toUpperCase();
			}
		} str = newString;
		return this;
	}

}
