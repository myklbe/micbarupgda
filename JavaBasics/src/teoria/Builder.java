package teoria;

public class Builder {

	public static void main(String[] args) {

		BuildProstokat(4, 5);

		// generateA();
		// generateByBuilder();
		/*
		 * String jest niemodyfikowalny
		 * 
		 * stringBuffer stringBuilder
		 * 
		 * 
		 * 
		 */
	}

	private static void generateA() {
		String a = "";
		for (int i = 0; i < 999; i++) {
			a += "a";
		}
		System.out.println(a);

	}

	private static void generateByBuilder() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 999; i++) {
			builder.append('a');
		}
	}

	private static void BuildLine(int a) {
		for (int i = 0; i < a; i++) {
			System.out.println("*");
		}
	}

	private static void BuildProstokat(int a, int b) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				builder.append('*');
			}
			builder.append('\n');

		}
		System.out.println(builder.toString());
	}

}
