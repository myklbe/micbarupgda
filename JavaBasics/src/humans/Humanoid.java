package humans;

public class Humanoid {
	private int limbs;
	
	public Humanoid(int limbs){
		this.limbs = limbs;
	}

	public int getLimbs() {
		return limbs;
	}

	public void setLimbs(int limbs) {
		this.limbs = limbs;
	}
	
}
