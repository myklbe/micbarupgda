package humans;

public enum UserSex {
	SEX_MALE("Man") ,
	SEX_FEMALE("Woman"), 
	SEX_OTHER("Other");
	
	private String commonName;
	
	private UserSex(String sex) {
		this.commonName = sex;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	
	
	
}
