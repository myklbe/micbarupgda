import org.junit.Test;

public class MetodyMatematykaTesty {

	public static void main(String[] args){
		
		metody.iloczynDwochLiczb(4, 10);   // <klasa>.<metoda(parametr)>
		// metody.najmniejszaLiczba(2,3);     // ta metoda jest private i nie mo�e by� eksportowana do innej klasy
		
		
		
	}
	// publiczna void, dowolna nazwa, (brak parametru)
	
	@Test   // - @adnotacja
	public void testIloczynu(){
		
		assert metody.iloczynDwochLiczb(1, 1) == 1; 
		assert metody.iloczynDwochLiczb(0, 10) == 0;
		assert metody.iloczynDwochLiczb(1, 10) == 10;
		assert metody.iloczynDwochLiczb(3, 4) == 12;
	}
	
	@Test 
	public void najmniejszaZtablicy() {
		
		int[] test = {2,3,-1,4,6,7,8,-1};
		assert metody.najmniejszaZtablicy(test) == -1;

	}
	
	@Test 
	public void szukaj() {
		
		int[] test = {2,3,-1,4,6,7,8,-1};
		assert metody.szukaj(test, -1) == 2;
		assert metody.szukaj(test, 2) == 0;
		assert metody.szukaj(test, 0) == -1;
	
	}
	@Test 
	public void najmnienajmniejszaZtrzech() {

		assert metody.najmniejszaZtrzech(1, 2, 3) == 1;
		assert metody.najmniejszaZtrzech(3, 2, 6) == 2;
		assert metody.najmniejszaZtrzech(2, 2, 3) == 2;

	}
	
}
