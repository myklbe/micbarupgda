package pl.org.pfig.cars;

public class Vehicle {
	
	private String name;
	private int tires;
	private String engine;
	
	public Vehicle(String name, int tires, String engine) {
		super();
		this.name = name;
		this.tires = tires;
		this.engine = engine;
	}	
	
	public String getName() {
		return name;
	}
	
	public int getTires() {
		return tires;
	}
	
}
