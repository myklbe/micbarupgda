package pl.org.pfig.data;

public class cat implements AnimalInterface, Soundable {
	private String name;

	public cat(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String getSound() {
		return "miau miau";
	}
	
}
