package pl.org.pfig.data;

public class Dog implements AnimalInterface {
	private String name;

	public Dog(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() { // reprezentacja obiektu jak String, czyli tekst
		return "Dog: " + this.name;
	}

	public Dog newDog(String name) {
		return new Dog(name);
	}
}
