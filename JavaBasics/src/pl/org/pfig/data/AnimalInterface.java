package pl.org.pfig.data;

public interface AnimalInterface {

	public String getName();

	public default int getLegs() {
		return 4;
	}

}
