import java.util.Scanner;

public class Array {

	
	public static void main(String[] args) {
		
		
		
		//simpleMethods();
		//createBoard();
		//pobierzElementy();	
		//tablicaImion();	
		
			
		}

	


	private static void tablicaImion() {
		String[] imiona = {"Maciek","Ania","Kasia","Bartek"};
		for (String i:imiona) {
			System.out.println(i);
		}
		for (int i = 0; i < imiona.length; i+=2) {
			System.out.println(imiona[i]);
		}
		
		System.out.println("Wypisz imiona na litere A: ");
		String a = "A";
		for (String i:imiona) {
			if (i.startsWith(a)){
			System.out.println(i);}
		}
		
	}



	private static void pobierzElementy() {
		Scanner cs = new Scanner(System.in);
		System.out.println("Podaj ilo�c elementow: .");
		int length = cs.nextInt();
		int elements[] = new int [length];
		for (int i=0; i<elements.length; i++) {
			System.out.println("Podaj wartosc nr ." +i);
			int a=cs.nextInt();
			elements[i]=a;
		}
	}
		
	

	private static void createBoard() {
		int[] tablica = {1,3,5,10};
		System.out.println(tablica[0]);
		
		for (int liczba:tablica) {			// dla ka�dego elementu w zbiorze zr�b {  }
			System.out.println(liczba); 
		}
		
		for (int i=0; i<tablica.length; i++) {
			System.out.println(tablica[i]);
		}
		
		for (int i=tablica.length-1; i>=0; i--) {  //
			System.out.println(tablica[i]);
		}
		
		String hello = "Hello World";
		char[]hell = hello.toCharArray();
		for (char i:hell){
			System.out.println(i);
		}
	}

	private static void simpleMethods() {
		int[] liczby = {2,3};
		
		System.out.println(liczby[0]);
		
		int[] liczby100 = new int[100]; // Tablica stu zer, od 0 do n-1
		System.out.println(liczby100[34]);
		
		boolean[] boole = new boolean[2];
		System.out.println(boole[0]);
		
		String[] slowa = new String[10];  // zbi�r slowa jest pusty
		System.out.println(slowa[3]);
	}

}
