import org.junit.Test;

public class GeoMatTest {
	

	@Test
	public void squaretest(){
		assert GeoMat.square(5) == 25;
		assert GeoMat.square(0) == 0;
	}
	@Test
	public void cubeAreaTest(){
		assert GeoMat.cubeArea(5) == 25*6;
		assert GeoMat.cubeArea(3) == 54;
	}
	@Test
	public void circleTest(){
		assert GeoMat.circle(4) < 16*3.15 && GeoMat.circle(4) > 16*3.14;
	}
	@Test
	public void roller(){
		assert GeoMat.roller(2,4) < 50.4; 
		assert GeoMat.roller(2,4) > 50.24;
		assert GeoMat.roller(2.4,8) < 2.4*2.4*8*3.15;
		assert GeoMat.roller(2.4,8) > 2.4*2.4*8*3.14;
	}
	@Test
	public void cone(){
		assert GeoMat.cone(5,6) == GeoMat.circle(5) * 2;
	}
	@Test
	public void cube(){
		assert GeoMat.cube(5) == 125;
	}
	
	@Test
	public void coneSquare(){
		assert GeoMat.coneSquare(5,6) == 50;

	}
	
	
}