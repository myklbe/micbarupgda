import org.junit.Test;

public class MyMathTest {

	public static void main(String[] args) {
		

	}

	@Test
	public void max(){
		assert MyMath.max(3.5d, 9.2d) == 9.2d;
		assert MyMath.max(3l, 9543l) == 9543;
		assert MyMath.max(2, 9) == 9 ;
		assert MyMath.max(3.5f, 9.2f) == 9.2f;
	}
	@Test
	public void min(){
		assert MyMath.min(3.5d, 9.2d) == 3.5d;
		assert MyMath.min(3l, 9543l) == 3;
		assert MyMath.min(2, 9) == 2 ;
		assert MyMath.min(3.5f, 9.2f) == 3.5f;
	}
	@Test
	public void abs(){
		assert MyMath.abs(-3.5d) == 3.5d;
		assert MyMath.abs(-3l) == 3;
		assert MyMath.abs(-2) == 2 ;
		assert MyMath.abs(-3.5f) == 3.5f;
	}
	@Test
	public void pow(){
		assert MyMath.pow(3.5, 2.0) == 12.25;
		assert MyMath.pow(3l,2l) == 9;
		assert MyMath.pow(2,2) == 4 ;
		assert MyMath.pow(3.5f, 3.0f) == 42.875f;
	}
}
