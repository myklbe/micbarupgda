
public class MyMath {

	public static void main(String[] args) {
		
		System.out.println(pow(3.0,3.0));
		
	}
	
	public static int max(int a, int b) {
		if (a<b) {
			return b;
		} else {
			return a; }	
	}

	public static long max(long a, long b) {
		if (a<b) {
			return b;
		} else {
			return a; }
	}
	public static double max(double a, double b) {
		if (a<b) {
			return b;
		} else {
			return a; }
	}
	public static float max(float a, float b) {
		if (a<b) {
			return b;
		} else {
			return a; }
	}
	
	public static int min(int a, int b) {
		if (a>b) {
			return b;
		} else {
			return a; }	
	}

	public static long min(long a, long b) {
		if (a>b) {
			return b;
		} else {
			return a; }
	}
	public static double min(double a, double b) {
		if (a>b) {
			return b;
		} else {
			return a; }
	}
	public static float min(float a, float b) {
		if (a>b) {
			return b;
		} else {
			return a; }
	}
	public static int abs(int a){
		if (a>0) {
			return a;
		} else { 
			return -a;
		}
	}
	public static long abs(long a){
		if (a>0) {
			return a;
		} else { 
			return -a;
		}
	}
	public static double abs(double a){
		if (a>0) {
			return a;
		} else { 
			return -a;
		}
	}
	public static float abs(float a){
		if (a>0) {
			return a;
		} else { 
			return -a;
		}
	}
	public static int pow(int a, int b){
		int pow = 1;
		for (int i=0; i<b; i++) {
			pow *= a;
		}
		return pow;
	}
	public static long pow(long a, long b){
		long pow = 1;
		for (int i=0; i<b; i++) {
			pow *= a;
		}
		return pow;
	}
	public static double pow(double a, double b){
		double pow = 1;
		for (int i=0; i<b; i++) {
			pow *= a;
		}
		return pow;
	}
	public static float pow(float a, float b){
		float pow = 1;
		for (int i=0; i<b; i++) {
			pow *= a;
		}
		return pow;
	}
}

