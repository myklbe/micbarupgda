/**
 * Created by Lenovo on 29.06.2017.
 */
public class Client {
    private Case aCase;
    private int pesel;

    public Client(Case aCase, int pesel) throws Exception{
        this.aCase = aCase;
        if (checkPeselLength(pesel)){
            this.pesel = pesel;
        } else {
            throw new IllegalArgumentException("PESEL musi posiadać dokładnie 11 cyfr");
        }
    }

    public Case getaCase() {
        return aCase;
    }

    public void setaCase(Case aCase) {
        this.aCase = aCase;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {


    }
    private boolean checkPeselLength(int pesel){
        String p = ""+pesel;
        if(p.length() != 11){
            return false;
        }
        return true;
    }
}
