package Time;

public class Main {

	public static void main(String[] args) {
		MyTime mt = new MyTime(0, 0, 87);

		MyTime mt2 = mt.nextSecond();
		MyTime mt3 = mt.nextMinute();
		MyTime mt4 = mt.nextHour();

		MyTime mt5 = mt.prevHour();
		MyTime mt6 = mt.prevMinute();
		MyTime mt7 = mt.prevSecond();

		System.out.println(mt5);
		System.out.println(mt6);
		System.out.println(mt7);
	}

}
