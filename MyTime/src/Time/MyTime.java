package Time;

public class MyTime {
	private int hour;
	private int minute;
	private int second;

	public MyTime(int hour, int minute, int second) {
		setTime(hour, minute, second);
	}

	public void setTime(int hour, int minute, int second) {
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}

	public void setHour(int hour) {
		if (hour >= 0 && hour <= 23) {
			this.hour = hour;
		}

	}

	public void setMinute(int minute) {
		if (minute >= 0 && minute <= 59) {
			this.minute = minute;
		}

	}

	public void setSecond(int second) {
		if (second >= 0 && second <= 59) {
			this.second = second;
		}

	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

	private String leadZero(int num) {
		if (num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}
	}

	@Override
	public String toString() {
		return leadZero(this.hour) + ":" + leadZero(this.minute) + ":" + leadZero(this.second);
	}

	public MyTime nextSecond() {
		int newSecond = second + 1, newMinute = minute, newHour = hour;
		if (newSecond > 59) {
			newSecond = 0;
			newMinute = minute + 1;
		}
		if (newMinute > 59) {
			newMinute = 0;
			newHour++;
		}
		if (newHour > 23) {
			newHour = 0;
		}
		return new MyTime(newHour, newMinute, newSecond);

	}

	public MyTime nextMinute() {
		int newMinute = minute + 1;
		int newHour = hour;
		if (newMinute > 59) {
			newMinute = 0;
			newHour = hour + 1;
		}
		if (newHour > 23) {
			newHour = 0;
		}
		return new MyTime(newHour, newMinute, second);
	}

	public MyTime nextHour() {
		int newHour = hour + 1;
		if (newHour > 23) {
			newHour = 0;
		}
		return new MyTime(newHour, minute, second);

	}

	public MyTime prevHour() {
		int newHour = hour - 1;
		if (hour == 0) {
			newHour = 23;
		}
		return new MyTime(newHour, minute, second);
	}

	public MyTime prevMinute() {
		int newHour = hour, newMinute = minute - 1;
		if (minute == 0) {
			newMinute = 59;
			newHour -= 1;
			if (hour == 0) {
				newHour = 23;
			}
		}
		return new MyTime(newHour, newMinute, second);
	}

	public MyTime prevSecond() {
		int newHour = hour, newMinute = minute, newSecond = second - 1;
		if (second == 0) {
			newSecond = 59;
			newMinute = minute - 1;
		}
		if (minute == 0) {
			newMinute = 59;
			newHour -= 1;
			if (hour == 0) {
				newHour = 23;
			}
		}
		return new MyTime(newHour, newMinute, newSecond);
	}

}
