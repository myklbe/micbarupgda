package dziedziczenie;

import org.junit.Test;

import figures.Circle;
import figures.Figure;
import figures.Square;

public class FigureTest {

	@Test
	public void FigureTest(){
	
		Circle circle = new Circle(6.0);
		Square square = new Square(4.5);
		assert circle.countArea() == 6.0*3.14*6.0;
		assert square.countArea() == 20.25;
		
		
	
	}
	
}
