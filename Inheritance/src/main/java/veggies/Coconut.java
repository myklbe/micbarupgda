package veggies;

public class Coconut extends Fruits {
	public Coconut(String name, double weight){
		super(name, weight);
	}

	@Override
	public void fruitname() {
		System.out.println("This fruit is called: " +this.getName());
		System.out.println("It weight: " +this.getWeight() +" gramm");
	}

}
