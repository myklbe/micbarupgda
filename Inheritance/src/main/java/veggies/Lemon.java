package veggies;

public class Lemon extends Fruits {
	private String sour;
	
	
	public Lemon(String name, double weight, String sour){
		super(name, weight);
		this.sour = sour;
	}
	
	@Override
	public void fruitname() {
		System.out.println("This fruit is called: " +this.getName());
		System.out.println("It weight: " +this.getWeight() +" gramm");
		System.out.println("and it tastes: " +sour);


	
	
	}
}
