package veggies;

public class Veggies {

	public static void main(String[] args) {
		Fruits lemon = new Lemon("Lemon", 2, "very sour");
		Fruits berries = new Berries("Berries", 4.33);
		Fruits mango = new Mango("Mango", 80);
		Fruits coconut = new Coconut("Cocont", 9.34);
		
		Fruits apple = new Apple("Apple", 456.32);
		Apple apple1 = new Apple("Apple1", 456.32);
		apple1.color = "red";
		
		System.out.println(apple1.color +" " +apple1.getName() +" " +apple1.getWeight());
		
		Fruits[] fruits = {lemon, berries, mango, coconut, apple};
		for(Fruits fruit: fruits){
			fruit.fruitname();
		}
	}
}
