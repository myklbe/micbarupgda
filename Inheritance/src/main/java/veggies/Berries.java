package veggies;

public class Berries extends Fruits {
	public Berries(String name, double weight){
		super(name, weight);
	}

	@Override
	public void fruitname() {
		System.out.println("This fruit is called: " +this.getName());
		System.out.println("It weight: " +this.getWeight() +" gramm");
	}
}
