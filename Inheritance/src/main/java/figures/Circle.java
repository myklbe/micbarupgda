package figures;

public class Circle implements Figure{
	private double r;
	
	public Circle(double r) {
		this.r = r;
	}
	public Circle(){
		
	}
	public double countArea() {
		return (3.14*r*r);
	}
	public double countCircumference(){
		return (2*3.14*r);
	}
}
