package figures;

public interface Figure {
	double countArea();
	double countCircumference();
}
