package Animal;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.sql.Savepoint;

public class AnimalExample {

	public static void main(String[] args) {

		Animal catStevens = new Cat();
		Cat cattie = new Cat();



		// Animal animal = new Cat(); // typem referencji jest interfejs 
		
		Animal[] animals = {new Cat(), new Dog()};
		for (Animal animal: animals){
			System.out.println(animal.makeNoise());
		}
		save5noisesToFile("noises.txt", new Cat());
	}
	
	public static void save5noisesToFile(String filename, Animal animal){
		try {
			PrintStream out = new PrintStream(filename);
			for(int i=0; i<5; i++){
				out.println(animal.makeNoise());
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
