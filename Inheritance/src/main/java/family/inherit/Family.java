package family.inherit;

public class Family {

	public static void main(String[] args) {
		Mother mother = new Mother("Grazyna"); // FamilyMember mother = new Mother("Grazyna"); = true
		Father father = new Father("Janus");
		Son son = new Son("Adam");
		Doughter doughter = new Doughter("Ania");

		/*mother.introduce();
		father.introduce();
		son.introduce();
		doughter.introduce();
		*/
		FamilyMember[] members = { mother, father, doughter, son };

		for (FamilyMember f : members) {
			f.introduce();
		}
	}
}
