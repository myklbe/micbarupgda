package family.inherit;

public class Son extends FamilyMember {
	
	public Son(String name) {
		super(name);
}
	
	
	@Override
	public void introduce() {
		System.out.println("I'm a Son. My name is " +this.getName());
}
}