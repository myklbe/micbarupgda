package family;

public class Family {

	public static void main(String[] args) {
		Mother mother = new Mother("Grazyna");
		introduce(mother);
		/*
		 * gdyby metoda introduce nie by�a statyczna musia�aby wygl�da� tak:
		 * Family family = new Family;
		 * family.introduce(mother);
		 */
		Father father = new Father("Janus");
		introduce(father); // odpowiednia meoda wywo�a si� poniewa� typ parametru jest taki sam jak w metodzie
		Son son = new Son("Adam");
		introduce(son);
		Doughter doughter = new Doughter("Ania");
		introduce(doughter);
	}

	public static void introduce(Mother mother) {
		System.out.println("I'm a mother. My name is " +mother.getName());
	}
	public static void introduce(Father father) {
		System.out.println("I'm a mother. My name is " +father.getName());
	}
	public static void introduce(Son son) {
		System.out.println("I'm a mother. My name is " +son.getName());
	}
	public static void introduce(Doughter dougther ) {
		System.out.println("I'm a mother. My name is " +dougther.getName());
	}

}
