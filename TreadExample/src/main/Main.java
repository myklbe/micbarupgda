package main;

import calculator.Calc;
import calculator.CalcInterface;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 19.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        Thread t = new Thread(new Example());
        t.start();

        new Thread(new AnotherThread(), "some Thread").start();
        System.out.println("KONIEC");

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("I am from code Runnable");
            }
        }).start();

        new Thread(() -> System.out.println("Message form FI")).start();
        new Thread(() -> {
            int a = 1, b = 3;
            System.out.println("Suma wynoso: " + (a + b));
        }).start();


        List<Person> p = new LinkedList<>();
        p.add(new Person(1));

        Calc c = new Calc();
        System.out.println(c.add(3, 4));

        System.out.println(c.oper(3, 5, new CalcInterface() {
            @Override
            public double doOperation(int a, int b) {
                return a + b;
            }
        }));
        System.out.println(c.oper(17, 2, (a, b) -> (a+b)));


    }


}
