package aminals;

import java.util.LinkedList;
import java.util.List;


public class Animal {
    public static void main(String[] args) {
        Animal animals = new Animal();
        animals.addAnimal("lion");
        System.out.println(animals.containsAnimal("lion"));
        System.out.println(animals.getAnimal(1));

    }



    List<String> animals = new LinkedList<>();

    public Animal(){}

    public void addAnimal(String animal){
        animals.add(animal);
    }
    public String getAnimal(int index){
        return (animals.size() > index) ? animals.get(index):"not here";
    }
    public boolean containsAnimal(String animal) {
        return animals.contains(animal);
    }
    public void addAnimal(String anil, AnimalInterface ai){
        String[] animal = ai.make(anil);
        for (int i = 0; i < animal.length; i++) {
            animals.add(animal[i]);
        }
    }
    public boolean containsAnimal(String animal, AnimalInterface ai) {
        String[] ani = ai.make(animal);
        List<String> a = new LinkedList<>();
        for (int i = 0; i < ani.length; i++) {
            if(!animals.contains(ani[i])){
                return false;
            }

        }
        return true;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "animals=" + animals +
                '}';
    }
}
