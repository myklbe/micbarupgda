package aminals;

@FunctionalInterface
public interface AnimalInterface {
    public String[] make(String animal);
}
