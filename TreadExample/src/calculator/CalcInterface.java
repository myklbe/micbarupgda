package calculator;


@FunctionalInterface
public interface CalcInterface {
    public double doOperation(int a, int b);
}
