package company;

import java.util.List;


public class Company implements FilterInterface, TransformInterface {
    private List<Employee> employeesList;

//    public Company(List<Employee> employeesList) {
//        this.employeesList = employeesList;
//    }

    public void filter(FilterInterface f, TransformInterface t) {
        for (Employee e: employeesList) {
            if(f.test(e.getName())){
                System.out.println(t.transform(e.getName()));
            }
        }
    }

    public void setEmployeesList(List<Employee> employeesList) {
        this.employeesList = employeesList;
    }

    @Override
    public boolean test(String s) {
        return false;

    }

    @Override
    public String transform(String s) {
        return null;
    }
}
