package company;

@FunctionalInterface
public interface TransformInterface {
    public String transform(String s);
}
