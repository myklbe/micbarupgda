package company;


@FunctionalInterface
public interface FilterInterface {
    public boolean test(String s);
}
