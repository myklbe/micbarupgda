package company;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 19.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        List<Employee> employees = new LinkedList<>();
        employees.add(new Employee("Paweł", "Testowy", 18, 3000));
        employees.add(new Employee("Piotr", "Przykładowy", 32, 10000));
        employees.add(new Employee("Julia", "Doe", 41, 4300));
        employees.add(new Employee("Przemysław", "Wietrzak", 56, 4500));
        employees.add(new Employee("Zofia", "Zaspa", 37, 3700));


        Company company = new Company();
        company.setEmployeesList(employees);

        company.filter( n -> n.startsWith("J"), t -> t.toUpperCase());




    }


// mam liste pracowników, musze przeiterować się przez imiona i wydrukować tylko tych na P,
    // metoda musi byc uniwersalna
    // metoda filter ma brać stringa sprawdzać czy jest obecny w liście, jeśli tak to zwracać
    //
}
