package commodities;

//

public class NewCommodity {
    private String name;
    private int buyValue;
    private int sellValue;
    private int amount;
    private int weight;

    public NewCommodity(String name, int buyValue, int sellValue, int amount, int weight) {
        this.name = name;
        this.buyValue = buyValue;
        this.sellValue = sellValue;
        this.amount = amount;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBuyValue() {
        return buyValue;
    }

    public void setBuyValue(int buyValue) {
        this.buyValue = buyValue;
    }

    public int getSellValue() {
        return sellValue;
    }

    public void setSellValue(int sellValue) {
        this.sellValue = sellValue;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void sellGoods(int amount) {
        int total = this.amount - amount;
        if (total < 0) {
            System.out.println("Nie mam tyle towaru");
        } else {
            this.amount = total;
        }

    }

}
