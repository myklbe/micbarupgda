package cities;

import Instance.*;

/**
 * Created by Lenovo on 28.06.2017.
 */
public abstract class NewCity {

    private Bank bank = Bank.createBank();

    private Gate gate;

    public NewCity(Bank bank, Gate gate) {
        this.bank = bank;
        this.gate = gate;
    }



    public void welcomeInTheCity(String cityName) {
        System.out.println("Witaj w mieście " + cityName);
    }




}
