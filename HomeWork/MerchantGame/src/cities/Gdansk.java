package cities;

import Instance.Bank;
import Instance.Gate;
import Instance.Market;
import Instance.Merchant;
import com.sun.org.apache.bcel.internal.generic.NEW;
import commodities.NewCommodity;

import java.util.Scanner;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Gdansk {

    private final String cityName = "Gdansk";
    private final NewCommodity fish = new NewCommodity("Ryby", 10, 6, 100, 1);
    private final NewCommodity coal = new NewCommodity("węgiel", 4, 1,10, 10);
    private final Merchant fishMerchant = new Merchant(1000, fish, "Sklep Rybny");
    private final Merchant coalMerchant = new Merchant(1000, coal, "Skup Węgla");
    private final Market market = new Market(fishMerchant,coalMerchant);
    private final Gate gate = new Gate();

    public Market getMarket() {
        return market;
    }

    public Gate getGate() {
        return gate;
    }

    void goToMarket(int choice){

    }



}
