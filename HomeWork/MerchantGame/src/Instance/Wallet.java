package Instance;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Wallet {
    private int money = 1000;

    private static Wallet wallet = null;

    private Wallet() {
    }

    public static Wallet makeWallet() {
        if (wallet == null) {
            wallet = new Wallet();
        }
        return wallet;
    }

    public int getMoney() {
        return money;
    }

    public void addMoney(int amount) {
        this.money += amount;

    }

    public void removeMoney(int amount) throws Exception {
        if (this.money - amount < 0){
            throw new Exception("Nie masz tyle pieniędzy");
        } else {
            this.money -= amount;
        }

    }
}
