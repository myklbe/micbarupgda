package Instance;

import cities.Cities;
import cities.Gdansk;

import java.util.Scanner;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Player {
    public static void main(String[] args) {

        Gdansk gdansk = new Gdansk();

        Scanner sc = new Scanner(System.in);
            welcomeInCity();
            chooseAction(sc.nextInt(), gdansk);

    }

    private static void welcometoGate() {
        System.out.println("Witaj w bramie do jakiego miasta chcesz się udać");
    }

    private static void welcomeInCity() {
        System.out.println("Witaj w grze, aktualnie jesteś w mieście Gdańsk. Co chcesz zrobić?" + "\n" +
                "1. Ide do innego miasta(brama)" + "\n" + "2. Ide na rynek" + "\n" + "3. Exit game");

    }

    private static void chooseAction(int next, Gdansk gdansk) {
        switch (next) {
            case 1: {
                Gate gate = gdansk.getGate();
                welcometoGate();
                gate.goToCity(next);
            }
            case 2: {
                Market market = gdansk.getMarket();
                welcomeToMarket();
                chooseMerchant(next, gdansk);
            }
            case 3: {
                System.out.println("Wycjśćie z gry");
                break;
            }
        }
    }

    private static void welcomeToMarket() {
        System.out.println("Witaj na rynku do jakiego kupca chcesz się udać");
    }
    public static void chooseMerchant(int number, Gdansk gdansk) {
        switch (number) {
            case 1:
                gdansk.getMarket().getMerchant1().merchantWelcome();
            case 2:
                gdansk.getMarket().getMerchant2().merchantWelcome();
            case 3:
                chooseAction(number, gdansk);
        }
    }

}


