package Instance;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Market {
    private Merchant merchant1;
    private Merchant merchant2;

    public Market(Merchant merchant1, Merchant merchant2) {
        this.merchant1 = merchant1;
        this.merchant2 = merchant2;
    }

    public Merchant getMerchant1() {
        return merchant1;
    }

    public void setMerchant1(Merchant merchant1) {
        this.merchant1 = merchant1;
    }

    public Merchant getMerchant2() {
        return merchant2;
    }

    public void setMerchant2(Merchant merchant2) {
        this.merchant2 = merchant2;
    }



    public void welcomeToMarket() {
        System.out.println("Do którego Kupca chcesz się udać " + "\n" +
                "1." + merchant1.getName() + " 2." + merchant2.getName());

    }

}
