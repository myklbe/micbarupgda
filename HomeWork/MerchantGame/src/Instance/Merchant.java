package Instance;

import commodities.NewCommodity;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Merchant {
    // posiada Instance.Money, Commodities, imie, rozmawia z urzytkownikiem, metody kup
    // sprzedaj,

    private int money = 1000;
    private NewCommodity commodity;
    private String name;
    private Wallet w = Wallet.makeWallet();

    public Merchant(int money, NewCommodity commodity, String name) {
        this.money = money;
        this.commodity = commodity;
        this.name = name;
    }

    public void merchantWelcome(){
        System.out.println("Witaj w moim kramiku! Sprzedaje: " + commodity.getName() );
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public NewCommodity getCommodity() {
        return commodity;
    }

    public void setCommodity(NewCommodity commodity) {
        this.commodity = commodity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NewCommodity buyCommodity(NewCommodity commodity){
        try {
            w.removeMoney(commodity.getAmount()*commodity.getBuyValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.commodity = commodity;
        return commodity;
    }
}
