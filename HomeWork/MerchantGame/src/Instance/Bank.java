package Instance;

import java.util.Random;

/**
 * Created by Lenovo on 28.06.2017.
 */
public class Bank {
    // pieniądze zdeponowane do banku nie moga zostac ukradzione,
    // zdarzenie napad na bank, zdeponowanie pieniedzy,

    private static int money = 0;

    public int getMoney() {
        return money;
    }

    private static Bank bank = null;

    private Bank(){}

    public static Bank createBank(){
        if (bank == null){
            bank = new Bank();
        }
        return bank;
    }

    public void depositMoney(int amount){
        money += amount;
    }
    public void withdrawMoney(int amount){
        int account = money - amount;
        if((account) < 0){
            System.out.println("Na twoim koncie nie ma wystarczającej liczby środków");
        } else {
            money = account;
        }
    }
    // ta metoda będzie użyta kiedy gracz wejdzie do Banku z poziomu miasta
    public void bankRobbery(){
        Random rand = new Random();
        if (rand.nextInt(10) == 5){
            money = money/2;
            System.out.println("Bank napadają rabusie, tracisz pieniądze");
        }
    }
    public void welcomeInBank(){
        System.out.println("Witamy w banku");
    }

}
