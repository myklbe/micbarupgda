package studends;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class University {
	File f = new File("resources/students.txt");

	private boolean isStudentExists(int index) {
		try (Scanner sc = new Scanner(f)) {
			String currentline;
			while (sc.hasNextLine()) {
				currentline = sc.nextLine();
				String[] st = currentline.split("\t");
				if (index == Integer.parseInt(st[0])) {
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return false;

	}

	public Student getStudent(int index) {
		try (Scanner sc = new Scanner(f)) {
			String currentline;
			while (sc.hasNextLine()) {
				currentline = sc.nextLine();
				String[] st = currentline.split("\t");
				if (isStudentExists(index)) {
					Student student = new Student(Integer.parseInt(st[0]), st[1], st[2], st[3],
							Double.parseDouble(st[4]));
					return student;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	public void putStudent(Student student) {
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pwr = new PrintWriter(fos);
			if (isStudentExists(student.getIndex())) {
				System.out.println("Student o podanym indexie ju� istnieje");
			} else {
				pwr.println(student);
			}
			pwr.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

}
