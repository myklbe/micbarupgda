/**
 * Created by Lenovo on 23.06.2017.
 */
public class BasicSportsman implements Sportsman {
    @Override
    public void prepare() {
        System.out.println("Na rozgrzewkę 100 pajacyków");
    }

    @Override
    public void doPumps(int number) {
        for (int i = 1; i <= number; i++) {
            System.out.println(i +" pompka");
        }
    }

    @Override
    public void doSquats(int number) {
        for (int i = 1; i <= number; i++) {
            System.out.println(i +" przysiad");
        };
    }

    @Override
    public void doCrunches(int number) {
        for (int i = 1; i <= number; i++) {
            System.out.println(i +" brzuszek");
        };
    }
}
