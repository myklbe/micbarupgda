/**
 * Created by Lenovo on 23.06.2017.
 */
public class WaterDrinking extends FitnessStudio {


    public WaterDrinking(Sportsman sportsman) {
        super(sportsman);


        System.out.println("Rememeber to drink water");

    }

    public void prepare() {
        newSportsman.prepare();
    }

    public void doPumps(int number) {
        newSportsman.doPumps(20);
        drinkWater();
    }

    public void doSquats(int number) {
        newSportsman.doSquats(30);
        drinkWater();
    }

    public void doCrunches(int number) {
        newSportsman.doCrunches(50);
        drinkWater();
    }

    public void drinkWater() {
        System.out.println("sip of water after round");
    }

}

