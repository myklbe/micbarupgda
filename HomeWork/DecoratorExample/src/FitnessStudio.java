/**
 * Created by Lenovo on 23.06.2017.
 */
public class FitnessStudio implements Sportsman {

    protected Sportsman newSportsman;

    public FitnessStudio(Sportsman sportsman){

        newSportsman = sportsman;
    }

    public void prepare(){
        newSportsman.prepare();
    }
    public void doPumps(int number){
        newSportsman.doPumps(20);
    }
    public void doSquats(int number){
        newSportsman.doSquats(30);
    }
    public void doCrunches(int number){
        newSportsman.doCrunches(50);
    }


}
