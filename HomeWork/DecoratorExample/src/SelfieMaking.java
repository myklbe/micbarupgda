/**
 * Created by Lenovo on 23.06.2017.
 */
public class SelfieMaking extends FitnessStudio {


    public SelfieMaking(Sportsman sportsman) {
        super(sportsman);

        System.out.println("You need a proof, take a shot!");
    }
    public void prepare(){
        newSportsman.prepare();
    }
    public void doPumps(int number){
        newSportsman.doPumps(20);
        selfie();
    }
    public void doSquats(int number){
        newSportsman.doSquats(30);
        selfie();
    }
    public void doCrunches(int number) {
        newSportsman.doCrunches(50);
        selfie();
    }
    public void selfie(){
        System.out.println("Smile!");
    }
}
