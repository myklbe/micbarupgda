/**
 * Created by Lenovo on 23.06.2017.
 */
public interface Sportsman {

    public void prepare();
    public void doPumps(int number);
    public void doSquats(int number);
    public void doCrunches(int number);



}
