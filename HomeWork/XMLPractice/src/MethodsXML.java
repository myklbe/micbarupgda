import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Lenovo on 17.06.2017.
 */
public class MethodsXML {
    private String filename = "staff.xml";

    private Document parseXML(String filename) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(filename);
        return doc;
    }

    public double getAvgSalary() throws IOException, SAXException, ParserConfigurationException {
        Document doc = parseXML(filename);
        NodeList salaryNode = doc.getElementsByTagName("salary");
        double sum = 0;
        for (int i = 0; i < salaryNode.getLength(); i++) {
            Node n = salaryNode.item(i);
            sum += Double.parseDouble(n.getTextContent());
        }
        double avg = sum / salaryNode.getLength();
        return avg;
    }

    public ArrayList<String> getNames() throws IOException, SAXException, ParserConfigurationException {
        Document doc = parseXML(filename);
        NodeList nodeList = doc.getElementsByTagName("staff");
        ArrayList<String> names = new ArrayList<>();
        for (int n = 0; n < nodeList.getLength(); n++) {
            Node node = nodeList.item(n);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String fName = element.getElementsByTagName("firstname").item(0).getTextContent();
                String lName = element.getElementsByTagName("lastname").item(0).getTextContent();
                names.add(fName + " " + lName);

            }
        }


        return names;


    }

    public void getMinMaxStaffId() throws IOException, SAXException, ParserConfigurationException {
        Document doc = parseXML(filename);
        NodeList nodeList = doc.getElementsByTagName("staff");
        Integer max = 0;
        Integer min = 0;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);
            Element e = (Element) n;
            Integer id = Integer.parseInt(e.getAttribute("id"));
            min = id;
            if (id > max) {
                max = id;
            } else if (id < min) {
                min = id;
            }
        }
        System.out.println("Min StaffID: " + min + " Max StaffID: " + max);
    }

    public void saveFile(Document doc, String filename) throws IOException, SAXException, ParserConfigurationException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(filename));
        transformer.transform(source, result);


    }

    public Document createDoc() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        return doc;
    }

    public Element addStudent(Document doc, Element rootElement, String name, String lastname, String year) {
        Element student = doc.createElement("STUDENT");
        rootElement.appendChild(student);
        student.appendChild(doc.createElement("NAME")).setTextContent(name);
        student.appendChild(doc.createElement("LASTNAME")).setTextContent(lastname);
        student.appendChild(doc.createElement("YEAR")).setTextContent(year);
        return rootElement;
    }

    public Element createRootElement(Document doc, String elementName) {
        Element rootElement = doc.createElement(elementName.toUpperCase());
        doc.appendChild(rootElement);
        return rootElement;
    }

    public Element addElement(Document doc, String elementName, Element rootElement) {
        Element element = doc.createElement(elementName.toUpperCase());
        rootElement.appendChild(element);
        return element;
    }

    public Element addChild(Document doc, Element rootElement, String tagName, String value){
        Element tag = doc.createElement(tagName.toUpperCase());
        rootElement.appendChild(tag);
        tag.setTextContent(value);
        return tag;

    }

    public Element addAttribute(Element el,String atrName, String value){
        el.setAttribute(atrName,value);
        return el;
    }
}
