/**
 * Created by Lenovo on 14.06.2017.
 */
public class Address {
    private String street;
    private String zipcode;
    private String number;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String city;
    private String country;

    public Address(String street, String zipcode, String number, String city, String country) {
        this.street = street;
        this.zipcode = zipcode;
        this.number = number;
        this.city = city;
        this.country = country;
    }

    public Address() {
    }

    @Override
    public String toString() {
        return "Address: " +
                "Street: " + street + ' ' +
                number + ", " +
                zipcode + ' ' +
                city + ", " +
                "Country: " + country;
    }
}
