package word;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Dict {
	private HashMap<String, String> dic = new HashMap<>();
	private String filename;

	public Dict(String filename) {
		readDict(filename);
		this.filename = filename;
	}

	public void readDict(String filename) {
		File f = new File(filename);
		try (Scanner sc = new Scanner(f)) {
			String currentLine;
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] d = currentLine.split("\t");
				dic.put(d[0], d[1]);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void addWord(String polishWord, String englishWord) {
		File f = new File(filename);
		try {
			FileOutputStream fos = new FileOutputStream(f, true);
			PrintWriter pwr = new PrintWriter(fos);
			if (!dic.containsKey(polishWord)) {
				pwr.println(polishWord.toLowerCase() + "\t" + englishWord.toLowerCase());
				pwr.close();
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void removeWord(String polishWord) {
		dic.remove(polishWord);
	}

	public String translateToEn(String polishContent) {
		if(dic.containsKey(polishContent)){
			return dic.get(polishContent).toLowerCase();
		}
		String a = "Nie ma takiego s�owa w s�owniku";
		return a;
	}

	public String translateToPl(String englishContent) {
		for (String key : dic.keySet()) {
			if (dic.get(key).equals(englishContent)) {
				return key;
			}
		}
		String a = "word does not exist in the dictionary";
		return a;
	}

}
