package pl.michal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.michal.entity.Specie;
import pl.michal.repository.SpecieRepository;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/species")
public class SpecieController {

    @Autowired
    private SpecieRepository specieRepository;

    @RequestMapping("/add")
    public Specie add(@RequestParam(name = "name") String name,
                      @RequestParam(name = "description") String description) {
        Specie s = new Specie();
        s.setName(name);
        s.setDescription(description);
        return specieRepository.save(s);
    }

    @RequestMapping("/show")
    public List<Specie> showAll() {
        return (List<Specie>) specieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Specie showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return specieRepository.findOne(myId);
    }








    @RequestMapping("/delete")
    public void delete(@PathVariable(value = "id")String id){
        long myId = Long.valueOf(id);
        specieRepository.delete(myId);
    }
    @RequestMapping("/update")
    public void update(@PathVariable(value = "id") String id,
                       @RequestParam(name = "name") String name,
                       @RequestParam(name = "description") String description){
        long myId = Long.valueOf(id);
        Specie s = specieRepository.findOne(myId);
        s.setName(name);
        s.setDescription(description);
        specieRepository.save(s);
    }

}
