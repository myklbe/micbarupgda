package pl.michal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.michal.entity.Animal;
import pl.michal.entity.Specie;
import pl.michal.repository.AnimalRepository;
import pl.michal.repository.SpecieRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@CrossOrigin
@RestController
@RequestMapping("/animal")
public class AnimalController {
    @Autowired
    private AnimalRepository animalRepository;
    @Autowired
    private SpecieRepository specieRepository;



    @RequestMapping("/add")
    public Animal add(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String desc,
                            @RequestParam(name = "image") String image,
                            @RequestParam(name = "specie") String specie) {
        Long specieId = Long.valueOf(specie);
        Specie s = specieRepository.findOne(specieId);
        Animal a = new Animal();
        a.setName(name);
        a.setDescription(desc);
        a.setImage(image);

        a.setSpecie(s);
        animalRepository.save(a);
        return a;
    }

    @RequestMapping("/show")
    public List<Animal> showAll() {
        return (List<Animal>)animalRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Animal showById(@PathVariable(name = "id") String id) {
       return animalRepository.findOne(Long.valueOf(id));
    }

    @RequestMapping("/delete{id}")
    public void delete(@PathVariable("id")String id){
        long myId = Long.valueOf(id);
        animalRepository.delete(animalRepository.findOne(myId));
    }

    @RequestMapping("/edit{id}")
    public void update(@PathVariable("id") String id,
                       @RequestParam(name = "name") String name,
                       @RequestParam(name = "description") String description,
                       @RequestParam(name = "image") String image,
                       @RequestParam(name = "specie") String specie){
        Long specieId = Long.valueOf(specie);
        Specie s = specieRepository.findOne(specieId);
        long myId = Long.valueOf(id);
        Animal animal = animalRepository.findOne(myId);
        animal.setName(name);
        animal.setDescription(description);
        animal.setImage(image);
        animal.setSpecie(s);
        animalRepository.save(animal);
    }
}
