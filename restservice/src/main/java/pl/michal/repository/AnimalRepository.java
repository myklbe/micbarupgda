package pl.michal.repository;

import org.springframework.data.repository.CrudRepository;
import pl.michal.entity.Animal;

/**
 * Created by Lenovo on 26.07.2017.
 */
public interface AnimalRepository extends CrudRepository<Animal, Long> {
}
