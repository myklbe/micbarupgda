package Intermediate;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Lenovo on 13.06.2017.
 */
public class Person {
    private String name;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    private String surname;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public Person(){}

    public Person randomPerson(String[] name, String[] surname){
        Random generator = new Random();
        Person person = new Person(name[generator.nextInt(name.length)],surname[generator.nextInt(surname.length)]);
        return person;
    }
    //musze mieć generator randomowej liczby który będzie określać który element z listy będzie drukować


}
