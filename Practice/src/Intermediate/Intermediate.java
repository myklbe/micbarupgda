package Intermediate;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 13.06.2017.
 */
public class Intermediate {

    public static void main(String[] args){

        //multi(2,10);
        int[] a = {1,2,3,3,6};
        int[] b = new int[a.length];
        //String string = Arrays.toString(square(a));

    }

    public static int repetitions(int[] array, int num){
        int rep = 0;
        for(int i = 0; i < array.length; i++){
            if (array[i] == num){
                rep++;
            }
        }return rep;
    }

    public static int repString(String text, char c){
        int rep = 0;
        char[] array = text.toCharArray();
        for(int i = 0; i < array.length; i++){
            if (array[i] == c) {
                rep++;
            }
        }return rep;
    }
    public static void multi(int a, int b){
        int c = 1;
        while(c*a < b){
            ++c;
            System.out.println(c*a);
        }
    }
    public static int[] copyArray(int[] array){
        int[] arr = new int[array.length];
        return arr = array.clone();
    }
    public <T> List<T> listFormArray(T[] array){
        List<T> list = new ArrayList<T>();
        for (T o: array) {
            list.add(o);
        } return list;
    }
    public static  int[] fromListtoArray(List<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
        public static int[] square ( int[] arr){
            int[] sqr = new int[arr.length];
            for (int i = 0; i < arr.length; i++) {
                sqr[i] = arr[i] * arr[i];

            }
            return sqr;
        }


}
