package Hard;

import java.util.*;

/**
 * Created by Lenovo on 13.06.2017.
 */
public class Hard {
    public static void main(String[] args){
        int[] a = {1,2,3,3,6,6,4,8,7,5,7,7};
        System.out.println(countAllNumbers(a));

    }
    public List<String> clone(LinkedList<String> origin){
        LinkedList<String> copy = (LinkedList) origin.clone();
        return copy;
    }

    public static List<Integer> lessThan(int a, int b){
         List<Integer> listA = new ArrayList<>();
         int c = 1;
         while (c*a < b){
              int d = c++ * a;
              listA.add(d);
         } return listA;
    }

    public static boolean isPrime(int num){
        for (int i=2; i < num; i++){
            if(num%i == 0 ){
                return false;
            }
        }return true;

    }
    public static HashMap<Integer, Integer> countAllNumbers(int[] arr) {

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i]) == false) {
                map.put(arr[i], 1);
            } else {
                map.put(arr[i], map.get(arr[i]) + 1);
            }
        }
        return map;
    }
}
