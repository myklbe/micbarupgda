SET SQL_SAFE_UPDATES = 0;

CREATE TABLE teacher (
	id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30),
    last_name VARCHAR(30)
    );

CREATE TABLE subject (
	id INT AUTO_INCREMENT PRIMARY KEY,
    teacher_id INT, FOREIGN KEY (teacher_id) REFERENCES teacher(id),
    subject_name VARCHAR(100)
    );

CREATE TABLE lesson (
	PRIMARY KEY (subject_id, lesson_nr), 
    subject_id INT, FOREIGN KEY (subject_id) REFERENCES subject(id),
    lesson_nr INT, FOREIGN KEY (lesson_nr) REFERENCES lesson_number(id)
    );

CREATE TABLE lesson_number (
	id INT AUTO_INCREMENT PRIMARY KEY, 
    start_time TIME,
    end_time TIME
    );
    
ALTER TABLE lesson ADD day_of_week ENUM ('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'); 
    
ALTER TABLE lesson DROP PRIMARY KEY;   
    
DROP TABLE lesson;

INSERT INTO teacher VALUES 
	(id, "Maria", "Kwiatkowska"),
    (id, "Stefan", "Niesiołowski");
    
INSERT INTO lesson_number VALUES
	(id, "08:00:00" , "08:45:00"),
    (id, "09:00:00" , "09:45:00"),
    (id, "10:00:00" , "10:45:00"),
    (id, "11:00:00" , "11:45:00"),
    (id, "12:00:00" , "12:45:00");
    
INSERT INTO subject VALUES 
	(id, 1, "WOS"),
    (id, 1, "Historia"),
    (id, 1, "J. Polski"),
    (id, 2, "Matematyka"),
    (id, 2, "Fizyka"),
    (id, 2, "Informatyka");
    
INSERT INTO lesson VALUES
	(1, 1),
    (2, 2),
    (4, 1),
    (5, 2),
    (3, 3),
    (6, 5);

CREATE VIEW lesson_full AS 
SELECT teacher.first_name, teacher.last_name, subject.subject_name, lesson_number.start_time, lesson_number.end_time FROM lesson 
LEFT JOIN subject ON subject_id = subject.id 
LEFT JOIN lesson_number ON lesson_number.id = lesson_nr 
LEFT JOIN teacher ON subject.id = lesson.subject_id AND teacher.id = subject.teacher_id;

SELECT first_name, last_name, start_time, end_time FROM lesson_full WHERE subject_name = "Matematyka";

SELECT subject.subject_name FROM lesson 
LEFT JOIN subject ON lesson.subject_id = subject.id
WHERE lesson_nr > 2 AND lesson_nr <= 5;


