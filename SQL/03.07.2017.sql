`order`SET SQL_SAFE_UPDATES = 0;

CREATE DATABASE Shop;
CREATE TABLE product (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name varchar(100),
    price DECIMAL(6,2),
    category_id INT, FOREIGN KEY(category_id) REFERENCES category(id)
    );
CREATE TABLE category (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30),
    description TEXT
    );

INSERT INTO category VALUES 
	(id, "Warzywa", "Promocja 50%"),
	(id, "Owoce", "1 gratis przy zakupie 3"),
	(id, "Mięso", "Przeterminowane mięso rabat 10%");

INSERT INTO product VALUES
	(id, "Ziemniak", 10, 1),
	(id, "Marchew", 2, 1),
	(id, "Groszek", 1, 1),
	(id, "Ogórek", 3, 1),
	(id, "Jabłko", 1, 2),
	(id, "Gruszka", 1, 2),
	(id, "Arbuz", 10, 2),
	(id, "Wieprzowina", 15, 3),
	(id, "Wołowina", 25, 3),
	(id, "Cielęcina", 32, 3);
    
SELECT * FROM product;

SELECT product.name, category.name FROM product LEFT JOIN category ON product.category_id = category.id;

SELECT c.name, count(*) FROM product p LEFT JOIN category c ON p.category_id = c.id GROUP BY category_id;

SELECT category.name, avg(price) FROM product LEFT JOIN category ON product.category_id = category.id GROUP BY category_id;

SELECT product.id, product.name, product.price, category.name FROM product LEFT JOIN category ON product.category_id = category.id;

CREATE VIEW product_full AS
		SELECT product.id, product.name AS product_name, product.price, category.name AS category_name FROM product LEFT JOIN category ON product.category_id = category.id;

SELECT * FROM product_full;