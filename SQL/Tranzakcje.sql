CREATE DATABASE bank;

CREATE TABLE `account` (
	id INT AUTO_INCREMENT PRIMARY KEY,
    number CHAR(32),
    balance DECIMAL(65,2)
    );
    
DROP TABLE acount;    
    
INSERT INTO `account` VALUES
	(id, "53 3003 2333 0000 0000 8983 9829", 100000),
	(id, "53 3003 2333 0000 0000 3244 8283", 100000);
    
START TRANSACTION;
UPDATE `account` SET balance = balance -1000 WHERE number = "53 3003 2333 0000 0000 8983 9829";
UPDATE `account` SET balance = balance + 1000 WHERE number = "53 3003 2333 0000 0000 3244 8283";
ROLLBACK;


SELECT * FROM `account`;
