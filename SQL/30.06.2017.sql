SET SQL_SAFE_UPDATES = 0;

CREATE DATABASE company;
CREATE TABLE worker (
	id int AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30),
    salary INT,
    age TINYINT
);

SELECT * FROM worker;


INSERT INTO worker (salary) VALUES ( "1000"), ("1000"), ("1500"), ("1500"), ("1500"), ("2000"),("2000"),("4000"),("4000");

SELECT max(salary) FROM worker;
SELECT min(salary) FROM worker;
SELECT avg(salary) FROM worker;

SELECT * FROM worker ORDER BY salary;

INSERT INTO worker (name, salary, age) VALUES ("Jan", "1000", "43");

SELECT * FROM worker ORDER BY salary DESC;

UPDATE worker SET age = "24" WHERE age IS NULL; 

SELECT count(id), salary FROM worker GROUP BY salary;

SELECT avg(salary), age FROM worker GROUP BY age;

SELECT id, salary FROM worker WHERE worker.salary = (SELECT max(salary) FROM worker);

SELECT * FROM worker ORDER BY salary DESC LIMIT 1;
SELECT * FROM worker ORDER BY salary LIMIT 2;

SELECT id, salary FROM worker WHERE worker.salary = (SELECT min(salary) FROM worker); 

SELECT name FROM worker WHERE 900 < ALL (SELECT salary FROM worker);


