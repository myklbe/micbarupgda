USE shop;

CREATE TABLE customer (
	id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30),
    last_name VARCHAR(30)
    );
    
CREATE TABLE `order` (
	id INT AUTO_INCREMENT PRIMARY KEY,
    date DATE,
    paid ENUM ('NEW', 'PAID', 'CANCELED', 'COMPLETION'),
    customer_id INT, FOREIGN KEY (customer_id) REFERENCES customer(id) 
    );
    
CREATE TABLE other_products (
	order_id INT, FOREIGN KEY (order_id)REFERENCES `order`(id),
    product_id INT, FOREIGN KEY (product_id)REFERENCES product(id),
    quantity INT UNSIGNED NOT NULL
    );
    
ALTER TABLE other_products ADD PRIMARY KEY (order_id, product_id);
    
INSERT INTO customer VALUE (id, "Jan", "Kowalski");

INSERT INTO `order` VALUES 
	(id, '2017-07-03', 'NEW', 1),
	(id, '2017-07-03', 'PAID', 1);

UPDATE `order` SET date = '2017-07-02' WHERE `order`.id = 2;

INSERT INTO other_products VALUES 
	(1, 1, 3),
	(1, 2, 2),
	(2, 4, 3),
	(2, 6, 2);

SELECT p.name, o.quantity FROM other_products o LEFT JOIN product p ON o.product_id = p.id WHERE o.order_id = 1;

CREATE OR REPLACE VIEW customer_order AS SELECT o.date, c.first_name AS `Name`, c.last_name AS `lastName`, o.paid FROM `order` o LEFT JOIN customer c ON o.customer_id = c.id;
CREATE OR REPLACE VIEW amount AS SELECT SUM(other_products.quantity * product.price) AS `amount` FROM other_products LEFT JOIN product ON other_products.product_id = product.id;
CREATE OR REPLACE VIEW order_quantity AS SELECT SUM(quantity) AS `quantity` FROM other_products;

SELECT * FROM customer_order, amount, order_quantity ;


