/**
 * Created by Lenovo on 14.06.2017.
 */
public enum UserSex {
    SEX_MALE(1, "Male"),
    SEX_FEMALE(2, "Female"),
    SEX_UNDEFINED(3, "Undefined");

    private String name;
    private int id;

    UserSex(int id, String name){
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
