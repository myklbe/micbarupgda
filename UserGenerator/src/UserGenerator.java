import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;


public class UserGenerator {


    public User getRandomUser() {
        UserSex us = UserSex.SEX_MALE;
        if (new Random().nextInt(2) == 0) {
            us = UserSex.SEX_FEMALE;
        }
        return getGetRandomUser(us);
    }

    public User getGetRandomUser(UserSex sex) {
        User currentUser = new User();
        currentUser.setSex(sex);
        String name = "";
        if (sex.equals(UserSex.SEX_MALE)) {
            name = getRandomLineFromFile("name_m.txt");
        } else {
            name = getRandomLineFromFile("name_f.txt");
        }
        currentUser.setName(name);
        currentUser.setSeconndName(getRandomSecondName());
        currentUser.setPhone(getRandomPhone());
        currentUser.setCCN(getRandomCCN());
        currentUser.setAddress(getRandomAddress());
        currentUser.setBirthDate(getBirthDate());
        currentUser.setPESEL(getValidPesel(getPESEL(currentUser)));


        return currentUser;
    }

    private int countLines(String filename) {
        int lines = 0;
        try (Scanner sc = new Scanner(new File(filename))) {
            while (sc.hasNextLine()) {
                lines++;
                sc.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return lines;
    }

    private String getRandomLineFromFile(String filename) {
        // wrzuć linie do tablicy i wygeneruj losowy element
        Random random = new Random();
        String[] lines = new String[countLines(filename)];
        try {
            Scanner scanner = new Scanner(new File(filename));
            while (scanner.hasNextLine()) {
                for (int i = 0; i < lines.length; i++) {
                    lines[i] = scanner.nextLine();
                }
            }
            return lines[random.nextInt(lines.length)];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getRandomSecondName() {
        return getRandomLineFromFile("lastname.txt");
    }

    private String getRandomPhone() {
        Random random = new Random();
        int[] phoneNumber = new int[9];
        for (int i = 0; i < phoneNumber.length; i++) {
            phoneNumber[i] = random.nextInt(10);
            if (phoneNumber[0] == 0) {
                phoneNumber[0] += 1;
            }
        }
        return Arrays.toString(phoneNumber).replaceAll(", ", "").replace("[", "").replace("]", "");
    }

    private String getRandomCCN() {
        Random random = new Random();
        String[][] ccn = new String[4][4];

        for (String[] i : ccn) {
            for (int j = 0; j < i.length; j++) {
                Integer rand = random.nextInt(10);
                i[j] = rand.toString();
            }
        }
        return Arrays.deepToString(ccn).replaceAll(", ", "").replaceAll("]", "").replace("[", " ").substring(2);
    }

    private Address getRandomAddress() {
        Address address = new Address();
        address.setCity(getRandomLineFromFile("city.txt"));
        address.setCountry("Poland");
        address.setCity(getRandomCity());
        address.setZipcode(getRandomZipCode());
        address.setNumber(getRandomAddressNumber());
        address.setStreet(getRandomLineFromFile("street.txt"));

        return address;
    }

    private String getRandomCity() {
        String citycode = getRandomLineFromFile("city.txt");
        String[] city = citycode.split("\t");
        return city[0];
    }

    private String getRandomZipCode() {
        String citycode = getRandomLineFromFile("city.txt");
        String[] city = citycode.split("\t");
        return city[1];
    }

    private String getRandomAddressNumber() {
        Random rand = new Random();
        Integer num = rand.nextInt(100);
        if (num == 0) {
            num += 1;
        }
        return num.toString();
    }

    private Date getBirthDate() {
        Integer m = generateMonth();
        Integer y = generateYear();
        Integer d = generateDay(m, y);
        Date birthDate = new Date(dateFromIntToString(d), dateFromIntToString(m), dateFromIntToString(y));
        return birthDate;
    }

    private int generateMonth() {
        Random random = new Random();
        Integer mon = random.nextInt(13);
        if (mon == 0) {
            mon += 1;
        }
        return mon;
    }

    private int generateDay(int mth, int y) {
        Random random = new Random();
        int day = 0;
        int[] months = {1, 3, 5, 7, 8, 10, 12};
        for (int m : months) {
            if (mth == m) {
                day = 1 + random.nextInt(31);
                return day;
            } else if (m == 2) {
                if (y % 4 == 0 && y % 100 != 0) {
                    day = 1 + random.nextInt(29);
                }
                day = 1 + random.nextInt(28);
            } else {
                day = 1 + random.nextInt(30);
            }
        }
        return day;

    }

    private int generateYear() {
        // rok urodzenia 1900 + random == max 2017
        Random rand = new Random();
        int year = 1900 + rand.nextInt(118);
        return year;
    }

    private String dateFromIntToString(int date) {
        String m = "" + date;
        if (m.length() == 1) {
            m = "0" + m;
            return m;
        }
        return m;
    }

    private Integer[] getPESEL(User currentUser) {
        Integer[] pesel = new Integer[11];
        pesel[0] = Integer.parseInt(currentUser.getBirthDate().getYear().substring(2,3));
        pesel[1] = Integer.parseInt(currentUser.getBirthDate().getYear().substring(3));
        pesel[3] = Integer.parseInt(currentUser.getBirthDate().getMonth().substring(1));
        pesel[4] = Integer.parseInt(currentUser.getBirthDate().getDay().substring(0,1));
        pesel[5] = Integer.parseInt(currentUser.getBirthDate().getDay().substring(1));
        if(Integer.parseInt(currentUser.getBirthDate().getYear()) >= 2000){
            pesel[2] = Integer.parseInt(currentUser.getBirthDate().getMonth().substring(0,1)) + 2;
        } else {
            pesel[2] = Integer.parseInt(currentUser.getBirthDate().getMonth().substring(0,1));
        }
        Random rand = new Random(10);
        Integer[] female = {0,2,4,6,8};
        Integer[] male = {1,3,5,7,9};
        if(currentUser.getSex() == UserSex.SEX_FEMALE){
            pesel[9] = female[rand.nextInt(5)];
        } else {
            pesel[9] = male[rand.nextInt(5)];
        }

        return pesel;
    }

    public String getValidPesel(Integer[] pesel){
        Random rand = new Random();
        pesel[6] = rand.nextInt(10);
        pesel[7] = rand.nextInt(10);
        pesel[8] = rand.nextInt(10);
        pesel[10] = rand.nextInt(10);
        int sum = pesel[0] +
                3*pesel[1] +
                7*pesel[2] +
                9*pesel[3] +
                pesel[4] +
                3*pesel[5] +
                7*pesel[6] +
                9*pesel[7] +
                pesel[8] +
                3*pesel[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        while(sum%10 != pesel[10]){
            pesel[10] = rand.nextInt(10);

        }
        return "PESEL: " +Arrays.toString(pesel).replaceAll(", ", "").replace("]", "").replace("[", "");
    }


}
