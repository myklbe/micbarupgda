package pl.michal.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Trade")
public class TradeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String symbol;
    private String side;
    private Date date;
    private Date timeEntry;
    private Date timeOut;
    private double priceEntry;
    private double priceOut;
    private double fees;
    private int shares;
    private double gross;

    public TradeEntity(String symbol, String side, Date date, Date timeEntry, Date timeOut, double priceEntry, double priceOut, double fees, int shares, double gross) {
        this.symbol = symbol;
        this.side = side;
        this.date = date;
        this.timeEntry = timeEntry;
        this.timeOut = timeOut;
        this.priceEntry = priceEntry;
        this.priceOut = priceOut;
        this.fees = fees;
        this.shares = shares;
        this.gross = gross;
    }
    public TradeEntity () {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTimeEntry() {
        return timeEntry;
    }

    public void setTimeEntry(Date timeEntry) {
        this.timeEntry = timeEntry;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public double getPriceEntry() {
        return priceEntry;
    }

    public void setPriceEntry(double priceEntry) {
        this.priceEntry = priceEntry;
    }

    public double getPriceOut() {
        return priceOut;
    }

    public void setPriceOut(double priceOut) {
        this.priceOut = priceOut;
    }

    public double getFees() {
        return fees;
    }

    public void setFees(double fees) {
        this.fees = fees;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public double getGross() {
        return gross;
    }

    public void setGross(double gross) {
        this.gross = gross;
    }

    @Override
    public String toString() {
        return "TradeEntity{" +
                "id=" + id +
                ", symbol='" + symbol + '\'' +
                ", side='" + side + '\'' +
                ", date=" + date +
                ", timeEntry=" + timeEntry +
                ", timeOut=" + timeOut +
                ", priceEntry=" + priceEntry +
                ", priceOut=" + priceOut +
                ", fees=" + fees +
                ", shares=" + shares +
                ", gross=" + gross +
                '}';
    }
}
