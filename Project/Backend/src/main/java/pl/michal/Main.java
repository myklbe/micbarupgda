package pl.michal;

import pl.michal.converter.ConverterCSV;
import pl.michal.model.TradeModel;
import pl.michal.util.TradeListUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lenovo on 08.08.2017.
 */
public class Main {
    public static void main(String[] args) {

        String path = "C:\\Users\\Lenovo\\workspace\\Project\\Backend\\src\\main\\resources\\TradeList.csv";
        List<TradeModel> tradeModelList = null;
        try {
            tradeModelList = ConverterCSV.CSVreader(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TradeListUtils.sortTradesBySymbolDateTime(tradeModelList);
        LinkedList<TradeModel> tempList = new LinkedList<>();
        tempList.add(tradeModelList.get(0));
        tempList.add(tradeModelList.get(1));

        for(TradeModel t: tradeModelList){
            System.out.println(t.getSymbol()+"  "+ t.getSide() +"  "+ t.getShare());

        }


//        System.out.println(tradeModelList);
//        System.out.println(TradeListUtils.createNewTrade(tempList));
//        System.out.println(tradeModelList.get(1).getShare());
        System.out.println(TradeListUtils.ConnectTrades(tradeModelList));

    }
}
