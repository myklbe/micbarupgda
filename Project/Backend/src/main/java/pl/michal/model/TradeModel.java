package pl.michal.model;



import javax.persistence.*;
import java.util.Date;


public class TradeModel {
    private long id;
    private String symbol;
    private String side;
    private double price;
    private int share;
    private double grossPL;
    private Date time;
    private Date date;
    private double gatewayFee;
    private String gateway;
    private String destination;
    private double regFee;
    private double clearingFee;
    private double activityFee;
    private String state;

    public TradeModel(){}

    public TradeModel(String symbol, String side, double price, int share, double grossPL, Date time, Date date, double gatewayFee, String gateway, String destination, double clearingFee, double activityFee, String state, double regFee) {
        this.symbol = symbol;
        this.side = side;
        this.price = price;
        this.share = share;
        this.grossPL = grossPL;
        this.time = time;
        this.date = date;
        this.gatewayFee = gatewayFee;
        this.gateway = gateway;
        this.destination = destination;
        this.clearingFee = clearingFee;
        this.activityFee = activityFee;
        this.state = state;
        this.regFee = regFee;

    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getShare() {
        return share;
    }

    public void setShare(int share) {
        this.share = share;
    }

    public double getGrossPL() {
        return grossPL;
    }

    public void setGrossPL(double grossPL) {
        this.grossPL = grossPL;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getGatewayFee() {
        return gatewayFee;
    }

    public void setGatewayFee(double gatewayFee) {
        this.gatewayFee = gatewayFee;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getClearingFee() {
        return clearingFee;
    }

    public void setClearingFee(double clearingFee) {
        this.clearingFee = clearingFee;
    }

    public double getActivityFee() {
        return activityFee;
    }

    public void setActivityFee(double activityFee) {
        this.activityFee = activityFee;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getRegFee() {
        return regFee;
    }

    public void setRegFee(double regFee) {
        this.regFee = regFee;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "TradeModel{" +
                "id=" + id +
                ", symbol='" + symbol + '\'' +
                ", side='" + side + '\'' +
                ", price=" + price +
                ", share=" + share +
                ", grossPL=" + grossPL +
                ", time=" + time +
                ", date=" + date +
                ", gatewayFee=" + gatewayFee +
                ", gateway='" + gateway + '\'' +
                ", destination='" + destination + '\'' +
                ", regFee=" + regFee +
                ", clearingFee=" + clearingFee +
                ", activityFee=" + activityFee +
                ", state='" + state + '\'' +
                '}';
    }
}
