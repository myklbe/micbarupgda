package pl.michal.converter;


import pl.michal.model.TradeModel;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class ConverterCSV {

    public static List<TradeModel> CSVreader(String filename) throws IOException, ParseException {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        List<TradeModel> tradeModelList = new LinkedList<>();
        String line = br.readLine();
        while (line != null){
            String[] tr = line.split(";");
            TradeModel tradeModel = new TradeModel();
            tradeModel.setSymbol(tr[0]);
            tradeModel.setSide(tr[1]);
            tradeModel.setPrice(convertToDouble(tr[2]));
            tradeModel.setShare(Integer.valueOf(tr[3]));
            tradeModel.setGrossPL(convertToDouble(tr[4]));
            tradeModel.setState(tr[5]);
            tradeModel.setGateway(tr[6]);
            tradeModel.setTime(convertToTime(tr[7]));
            tradeModel.setGatewayFee(convertToDouble(tr[8]));
            tradeModel.setDestination(tr[9]);
            tradeModel.setRegFee(convertToDouble(tr[10]));
            tradeModel.setDate(convertToDate(tr[11]));
            tradeModel.setClearingFee(convertToDouble(tr[12]));
            tradeModel.setActivityFee(convertToDouble(tr[13]));
            tradeModelList.add(tradeModel) ;
        }
        return tradeModelList;
    }

    private static Date convertToDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date d = sdf.parse(date);
        return d;
    }
    private static Date convertToTime(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        Date t = sdf.parse(time);
        return t;
    }
    private static double convertToDouble(String value) throws ParseException {
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        return nf.parse(value).doubleValue();
    }

}
