var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8080/';

app.config(function($routeProvider){
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/main', {
            templateUrl: path + 'main.html'
        })
        .when('/showByDate', {
            templateUrl: path + 'TradeTable.html',
            controller: 'showByDateController'
        })
        .when('/upload', {
            templateUrl: path + 'main.html',
            controller: 'uploadForm'
        
    });
});

app.controller('uploadForm', function($scope, $http){
    $http({
        url: url + 'upload',
        dataType: 'json'
    }).then(function(success){
        $scope.message;
    }, function(error){
        $scope.error(error);
    });
});


app.controller('showByDateController', function($scope, $http, $filter){
    $scope.showByDate = function() {
        $scope.from = $filter('date')($scope.from, "yyyyMMdd");
        $scope.to = $filter('date')($scope.to, "yyyyMMdd");
        $http({
            url: url + 'showByDate',
            method: 'GET',
            dataType: 'json',
            params: {
                startDate: $scope.from,
                endDate: $scope.to
            }
        }).then(function(success){
            console.log($scope.from, $scope.to);
            $scope.trades = success.data;
            console.log($scope.trades);
        }, function(error) {
            console.error(error);
        });
    }
});
