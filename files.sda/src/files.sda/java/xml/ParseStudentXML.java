package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static xml.ParseXML.*;

public class ParseStudentXML {

	private static final String FILE_NAME = "class.xml";

	public static void main(String[] args) {

		try {
			Document doc = parseFile(FILE_NAME);
			printStudentsInfo(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static void printStudentsInfo(Document doc) {
		NodeList students = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < students.getLength(); i++) {
			Node student = students.item(i);
			ParseStudentXML.printStudentInfo(student);
		}
	}

	static void printStudentInfo(Node studentNode) {
		System.out.println("\nCurrent Element :" + studentNode.getNodeName());
		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) studentNode;
			System.out.println("Student roll no : " + student.getAttribute("rollno"));
			System.out.println("First Name : " + getText(student, "firstname"));
			System.out.println("Last Name : " + getText(student, "lastname"));
			System.out.println("Nick Name : " + getText(student, "nickname"));
			System.out.println("Marks : " + getText(student, "marks"));
		}
	}

}