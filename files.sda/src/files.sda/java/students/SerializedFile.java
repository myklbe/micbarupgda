package students;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializedFile implements IfIle {
	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Student> studentList) {

		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
			out.writeObject(studentList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public List<Student> load() {

		;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
			List<Student> students = (List<Student>) ois.readObject();
			return students;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}
	// ta metoda jest czytelna tylko dla javy a w klasie studenta(obiektu kt�ry zapisujemy/odczytujemy) musimy implementowa� interfejs serializable - wyskakuje b��d)
}
