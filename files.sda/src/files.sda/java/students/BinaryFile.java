package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IfIle {
	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Student> studentList) {

		DataOutputStream dos = null;
		try {
			FileOutputStream fos = new FileOutputStream(FILE_NAME);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			dos = new DataOutputStream(bos);

			for (Student st : studentList) {
				dos.writeInt(st.getNumerIndeksu());
				dos.writeUTF(st.getImie());
				dos.writeUTF(st.getNazwisko());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (dos != null) {
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public List<Student> load() {
		List<Student> students = new ArrayList<>();
		try {
			DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)));
			while(dis.available() > 0){ 
				int indexID = dis.readInt();
				String name = dis.readUTF();
				String surname = dis.readUTF();
				Student st1 = new Student(indexID, name, surname);
				students.add(st1);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return students;
	}

}
