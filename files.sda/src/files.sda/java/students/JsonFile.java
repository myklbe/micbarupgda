package students;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonFile implements IfIle {

	private ObjectMapper mapper = new ObjectMapper();
	private final static String filename = "studenci.json";

	@Override
	public void save(List<Student> studentList) {
		try {
			mapper.writeValue(new File(filename), studentList);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Student> load() {
		try {
			List<Student> students = mapper.readValue(new File(filename), new TypeReference<List<Student>>() {
			});
			return students;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
