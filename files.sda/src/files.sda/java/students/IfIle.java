package students;

import java.util.List;

public interface IfIle {

	void save(List<Student> studentList);

	List<Student> load();

}