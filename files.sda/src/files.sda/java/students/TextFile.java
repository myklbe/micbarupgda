package students;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements IfIle {

	private final static String FILE_NAME = "students_text.txt";
	
	/* (non-Javadoc)
	 * @see students.IfIle#save(java.util.List)
	 */
	@Override
	public void save(List<Student> studentList){
		try (PrintStream printStream = new PrintStream(FILE_NAME)){
			for (Student student : studentList){
				printStream.println(student.getNumerIndeksu() + " " +student.getImie() + " " + student.getNazwisko());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	/* (non-Javadoc)
	 * @see students.IfIle#load()
	 */
	@Override
	public List<Student> load(){
		List<Student> students = new ArrayList<>();
		
		try (Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(FILE_NAME)))){
			while (sc.hasNextLine()){
				String currentLine = sc.nextLine();
				String[] st = currentLine.split(" ");
				Student st1 = new Student(Integer.parseInt(st[0]), st[1], st[2]);
				students.add(st1);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return students;
	}
}
