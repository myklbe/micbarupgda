package students;

import static xml.ParseXML.getText;
import static xml.ParseXML.parseFile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseXML;

public class XMLFile implements IfIle {

	@Override
	public void save(List<Student> studentList) {
		
		try {
			Document doc = ParseXML.newDocument();
			createContent(doc, studentList);
			ParseXML.saveFile(doc, "students.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	public void createContent(Document doc, List<Student> studentList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);
		
		for(Student s: studentList){
			Element studentElement = doc.createElement("student");
			studentElement.setAttribute("index", String.valueOf(s.getNumerIndeksu()));
			students.appendChild(studentElement);
			
			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(s.getImie()));
			students.appendChild(name);
			
			Element surname = doc.createElement("surname");
			surname.appendChild(doc.createTextNode(s.getNazwisko()));
			students.appendChild(surname);
		}
		}

	@Override
	public List<Student> load() {
		LinkedList<Student> studentList = new LinkedList<>();
		try {
			Document doc = ParseXML.parseFile("students.xml");
			NodeList students = doc.getElementsByTagName("student");
			
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Student s = parse(students);
				studentList.add(s);
				
			
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return studentList;
	}

	private Student parse(NodeList studentNode) {
		  
		Element student = (Element) studentNode;
		int index = Integer.parseInt(student.getAttribute("index"));
		String name = getText(student, "name");
		String surname = getText(student, "surname");
		return new Student(index, name, surname);	
		}
		
	
}