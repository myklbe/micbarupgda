package students;

import java.io.Serializable;
import java.util.Map;

public class Student implements Serializable {
	private int indexID;
	private String name;
	private String surname;

	public Student(int indexID, String name, String surname) {
		super();
		this.indexID = indexID;
		this.name = name;
		this.surname = surname;
	}
	public Student(){
		
	}

	public int getNumerIndeksu() {
		return indexID;
	}

	public void setNumerIndeksu(int numerIndeksu) {
		this.indexID = numerIndeksu;
	}

	public String getImie() {
		return name;
	}

	public void setImie(String imie) {
		this.name = imie;
	}

	public String getNazwisko() {
		return surname;
	}

	public void setNazwisko(String nazwisko) {
		this.surname = nazwisko;
	}

	@Override
	public String toString() {
		return "Student: " + indexID + " " + name + " " + surname;
	}

}
