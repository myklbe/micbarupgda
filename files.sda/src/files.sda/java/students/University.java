package students;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();

	public University() {}
	
	public University(List<Student> studentsList){
		for(Student s : studentsList){
			students.put(s.getNumerIndeksu(), s);
		}
	}
	
	
	public void addStudent(int indexID, String name, String surname) {
		Student student = new Student(indexID, name, surname);
		students.put(student.getNumerIndeksu(), student);
	}

	public boolean studentExists(int indexID) {
		// delegacja
		return students.containsKey(indexID);
	}

	public Student getStudent(int indexID) {
		// delegacja
		return students.get(indexID);

	}

	public int studentNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getNumerIndeksu() + ":" + s.getImie() + " " + s.getNazwisko());
		}
	}
	public List<Student> getStudentList(){
		List<Student> studentList = new ArrayList<>();
		for(Student student: students.values()){
			studentList.add(student);
		}return studentList;
		// najkr�cej return new LinkedList<>(students.values()); albo tak: 
		
	}
	
}
