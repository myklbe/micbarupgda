package exercices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import TDD.EuclidesAlgorythmTest;

public class Exercises {

	public static void main(String[] args) {

//		System.out.println(Arrays.toString(increseDecrese(5, 15)));
	}

	public int absoluteValue(int num) {
		if (num < 0) {
			return -num;
		}
		return num;
	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		if (day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] days = { "niedziela", "poniedzialek", "wotorek", "sroda", "czwartek", "piatek", "sobota" };
		return days[day - 1];
	}

	public String determineMonth(int month) throws IllegalArgumentException {
		if (month > 12 || month < 1) {
			throw new IllegalArgumentException();
		}
		String[] months = { "january", "february", "march", "april", "may", "june", "july", "august", "september",
				"october", "november", "december" };
		return months[month - 1];
	}

	public int euclidesAlgorythm(int a, int b) throws ArithmeticException {
		if (a == 0 && b == 0) {
			throw new ArithmeticException("cannot divide by 0");
		}
		int c;
		while (b != 0) {
			c = a % b;
			a = b;
			b = c;
		}
		if (a < 0) {
			return -a;
		}
		return a;
	}

	public int timeInSeconds(int h, int m, int s) {
		if (h < 0 || m < 0 || s < 0) {
			throw new IllegalArgumentException("Value/s is less than 0");
		}
		int secondsSum = h * 3600 + m * 60 + s;
		if (secondsSum < 0) {
			throw new IllegalArgumentException("Outcome is bigger then Interger range");
		}
		return secondsSum;
	}

	public int[] increseDecrese(int a, int b) throws IllegalArgumentException {
		int[] array = { a };
		if (a > b || a > 255 || a < 0 || b > 255 || b < 0) {
			throw new IllegalArgumentException("Wrong numbers");
		} else if (a == b) {
			return array;
		} else if (a%2 == 1){
			a+=1;
		}
		
		for(int i = 0; a <= b; i++){
			array = Arrays.copyOf(array, array.length + 1);
			array[i] = a;
			a += 2;
		}
		array[array.length - 1] = array[array.length - 2] - 1;
		
		for(int i = array.length; array[array.length-1] >= array[0]; i++){
			array = Arrays.copyOf(array, array.length + 1);
			array[i] = array[array.length - 2] - 2;
		}
		return array;
		
	}
	
}
