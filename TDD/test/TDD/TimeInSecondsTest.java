package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;

public class TimeInSecondsTest {
	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	// je�li podamy tylko godziny, minuty, sekundy, godziny i sekundy, minuty i
	// godziny, minuty i sekundy
	// czy wynik jest prawid�owy, je�li kt�ra� z warto�� b�dzie ujemna, je�li
	// b�dzie wykraczac poza zasi�c
	// wszystkie warto�ci zerowe,
	@Test
	public void ifOneOrTwoValuesAreMissingExpectedPossitiveResult() {
		int[][] dataSet = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 0 }, { 0, 1, 1 }, { 1, 0, 1 } };
		for (int[] data : dataSet) {
			assertEquals((data[0] * 3600 + data[1] * 60 + data[2]), e.timeInSeconds(data[0], data[1], data[2]));
		}
	}

	@Test
	public void whenOneOrMoreValuesAreNegativeIllegalArgumentExceptionExpected() {
		int[][] dataSet = { { -1, 1, 1 }, { 1, -1, 1 }, { 1, 1, -1 }, { -1, -1, 1 }, { 1, -1, -1 }, { -1, -1, -1 },
				{ -1, 1, -1 }, };
		IllegalArgumentException iae = null;
		try {
			for (int[] data : dataSet) {
				e.timeInSeconds(data[0], data[1], data[2]);
			}
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		assertNotNull("Exception not occured", iae);
	}
	@Test
	public void whenAllValuesAreZeroExpectedZeroOutcome(){
		int h= 0, m = 0, s = 0;
		int expected = 0;
		assertEquals(expected, e.timeInSeconds(h, m, s));
	}
	@Test
	public void whenOutcomeValueIsOutIntegerRangeExceptionExpected(){
		int h= 1, m = 1, s = Integer.MAX_VALUE;
		IllegalArgumentException iae = null;
		try {	
			e.timeInSeconds(h, m, s);
	} catch (IllegalArgumentException i){
		iae = i;
	}
		assertNull("Exception occured", iae);
	}

}
