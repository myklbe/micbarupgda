package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;

public class AbsoluteValueTest {
	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenPossitiveValueIsGivenPossitiveValueISExpected() {
		int arg = 5;
		int expected = 5;
		assertEquals(expected, e.absoluteValue(arg));
	}

	@Test
	public void whenNegativeValueIsGivenPositiveValueIsExpected() {
		int arg = -5;
		int expected = 5;
		assertEquals(expected, e.absoluteValue(arg));
	}

	@Test
	public void whenZeroIsGivenZeroIsExpected() {
		int arg = 0;
		int expected = 0;
		assertEquals(expected, e.absoluteValue(arg));
	}

}
