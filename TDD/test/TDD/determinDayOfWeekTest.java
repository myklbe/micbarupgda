package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;

public class determinDayOfWeekTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

	@Test
	public void whenNumberOfDayIsGivenSpecificDayExpected() {
		int day = 1;
		String expected = "niedziela";
		assertEquals(expected, e.determineDayOfWeek(day));
	}

	@Test
	public void numberGivenInRange() {
		int[] day = { 1, 7 };
		String[] expected = { "niedziela", "sobota" };
		for (int i = 0; i < day.length; i++) {
			assertEquals(expected[i], e.determineDayOfWeek(day[i]));
		}
	}

	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int data = -14;
		IllegalArgumentException iae = null;
		try {
			e.determineDayOfWeek(data);
		} catch (IllegalArgumentException e) {
			iae = e;

		}
		assertNotNull("Exception not occured", iae);
	}

}
