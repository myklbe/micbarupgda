package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;

public class EuclidesAlgorythmTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}
	// pierwsza 0, druga 0, obie 0, pierwsza -, druga -, obie -, takie liczby ze
	// zwraca 1, nie moze zwrocic ujemnej,
	// czy wyrzuca dobra liczbe,

	@Test
	public void whenTwoPositiveIntegerGivenPositiveIntegerExpected() {
		int a = 12;
		int b = 4;
		int expected = 4;
		assertEquals(expected, e.euclidesAlgorythm(a, b));
	}

	@Test
	public void whenFirstNumberIsZeroExpectedSecondNumber() {
		int a = 0;
		int b = 12;
		int expeceted = 12;
		assertEquals(expeceted, e.euclidesAlgorythm(a, b));
	}

	@Test
	public void whenFirstNumberIsnegativeExpectedPossitiveOutcome() {
		int a = -6;
		int b = 12;
		int expeceted = 6;
		assertEquals(expeceted, e.euclidesAlgorythm(a, b));
	}

	@Test
	public void whenSecondNumberIsnegativeExpectedPossitiveOutcome() {
		int a = 6;
		int b = -18;
		int expeceted = 6;
		assertEquals(expeceted, e.euclidesAlgorythm(a, b));
	}

	@Test(expected=ArithmeticException.class)
	public void whenNumbersAreZeroArithmeticException() {
		int a = 0;
		int b = 0;
		ArithmeticException ae = null;
		try {
			e.euclidesAlgorythm(a, b);
		} catch (ArithmeticException i) {
			ae = i;
		}
		assertNotNull("Exception not occured", ae);
	}

	@Test
	public void whenSecondNumberZeroFirstNumberExpected() {
		int a = 12;
		int b = 0;
		int expeceted = 12;
		assertEquals(expeceted, e.euclidesAlgorythm(a, b));

	}

	public void whenOnlyOneOutcomePossibleNumberOneExpected() {
		int a = 4;
		int b = 13;
		int expected = 1;
		assertEquals(expected, e.euclidesAlgorythm(a, b));
	}

	@Test
	public void whenTwoNumbersIsNegativeExpectedPossitiveOutcome() {
		int a = -6;
		int b = -18;
		int expeceted = 6;
		assertEquals(expeceted, e.euclidesAlgorythm(a, b));
	}

}
