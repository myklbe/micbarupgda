package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;



public class DetermineMonthTest {
	Exercises e;
	@Before
	public void init(){
		e = new Exercises();
	}
	// sprawdz watro�ci poza rangem, warto�� srodkowa, exception, 
	
	@Test
	public void numberGivenInRange() {
		int[] month = { 1, 12 };
		String[] expected = { "january", "december" };
		for (int i = 0; i < month.length; i++) {
			assertEquals(expected[i], e.determineMonth(month[i]));
		}
	}
	@Test
	public void whenNumberOfMonthIsGivenSpecificMonthExpected() {
		int month = 5;
		String expected = "may";
		assertEquals(expected, e.determineMonth(month));
	}
	@Test
	public void whenOutOfRangeNumberIsGivenExceptionExpected(){
		int data = 13;
		IllegalArgumentException iae = null; 
		try {
			e.determineMonth(data);
		} catch (IllegalArgumentException e) {
			iae = e;
			
		}
		assertNotNull("Exception not occured", iae);
	}

}
