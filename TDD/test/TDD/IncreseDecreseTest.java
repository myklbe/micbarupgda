package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercices.Exercises;

public class IncreseDecreseTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenArgumentAIsOutOfRangeExeptionExpected() {
		IllegalArgumentException ex = null;
		int[] arrayOfA = { -1, 256, 1000, -1000 };
		int b = 8;
		for (int a : arrayOfA) {
			try {
				e.increseDecrese(a, b);
			} catch (IllegalArgumentException e) {
				ex = e;
			}
			assertTrue(ex != null);
			ex = null;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentBIsOutOfRangeExeptionExpected() {
		IllegalArgumentException ex = null;
		int a = 3;
		int[] arrayOfB = { -123, 257, -1, 1000 };
		for (int b : arrayOfB) {
			try {
				e.increseDecrese(a, b);
			} catch (IllegalArgumentException e) {
				ex = e;
			}
			assertTrue(ex != null);
			ex = null;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentAreInIncorrentOrderExceptionExpected() {
		int a = 8, b = 3;
		e.increseDecrese(a, b);
	}

	@Test
	public void whenArgumentsAreProperExpectedArrayResult() {
		int a = 3, b = 8;
		int[] expected = { 4, 6, 8, 7, 5, 3 };
		assertArrayEquals(expected, e.increseDecrese(a, b));
	}

	@Test
	public void whenArgumentsAreTheSameExpectOneItemArray() {
		int arg = 5;
		int[] ex = { 5 };
		assertArrayEquals(ex, e.increseDecrese(arg, arg));
	}
}
