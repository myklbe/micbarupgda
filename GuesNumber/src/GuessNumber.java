import java.io.InputStream;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Lenovo on 20.06.2017.
 */
public class GuessNumber {

    public void guessNumber(){
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        int number = rand.nextInt(101);
        int count = 10;
        System.out.println("Guess number from 0 to 100");
        for (int i = 0; i <= 10; i++) {
            if(Integer.parseInt(sc.nextLine()) == number){
                System.out.println("Congrats! You Guessed!");
                break;
            } else {
                System.out.println("You're wrong, try again. Trials left: " + --count);
            }
        }


    }

}
