package pl.michal.moviedatabase.entity;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created by Lenovo on 24.07.2017.
 */
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "movie")
    private String name;

    public Movie () {}

    public Movie(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
