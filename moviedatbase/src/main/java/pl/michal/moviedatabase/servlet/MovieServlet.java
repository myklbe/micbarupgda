package pl.michal.moviedatabase.servlet;

import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.michal.moviedatabase.entity.Movie;
import pl.michal.moviedatabase.util.HibernateUtil;

@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
//        SessionFactory sf = new Configuration().configure().buildSessionFactory();
        if (action.equals("add")) {
            String name = request.getParameter("movieName");
            Session s = HibernateUtil.openSession();
            Movie m = new Movie(name);
            Transaction t = s.beginTransaction();
            s.save(m);
            t.commit();

        } else if (action.equals("edit")){
            String name = request.getParameter("movieName");
            int id = Integer.valueOf(request.getParameter("id"));
            Movie movie = new Movie();
            movie.setId(id);
            Session s = HibernateUtil.openSession();
            Transaction t = s.beginTransaction();
            s.update(movie);
            t.commit();

        }
        request.getRequestDispatcher("MovieServlet?action=show").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session s = HibernateUtil.openSession();
        String action = request.getParameter("action");

        if(action.equals("show")){
            Transaction t = s.beginTransaction();
            List<Movie> movies = s.createQuery("FROM Movie").list();
            t.commit();
            request.setAttribute("movies", movies);
            request.getRequestDispatcher("movies.jsp").forward(request, response);
        } else if (action.equals("edit")){
            int id = Integer.parseInt(request.getParameter("id"));
            Movie m = s.get(Movie.class, id);
            request.setAttribute("movie", m);
            request.getRequestDispatcher("update.jsp").forward(request,response);

        } else if (action.equals("delete")){
            int id = Integer.valueOf(request.getParameter("id"));
            Movie m = new Movie();
            m.setId(id);
            Transaction t = s.beginTransaction();
            s.delete(m);
            t.commit();

            request.getRequestDispatcher("MovieServlet?action=show").forward(request, response);
        }

    }
}
