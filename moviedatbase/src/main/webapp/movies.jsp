<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp" />
<section>
    <table>
        <c:forEach items="${movies}" var="movie">
            <tr>
                <td><c:out value="${movie.id}" /></td>
                <td><c:out value="${movie.name}" /></td>
                <td><a href="MovieServlet?action=edit&id=<c:out value="${movie.id}" />"> Edytuj</a>
                    <a href="MovieServlet?action=delete&id=<c:out value="${movie.id}" />"> Usuń</a></td>
            </tr>
        </c:forEach>
    </table>
</section>
<c:import url="footer.jsp" />
