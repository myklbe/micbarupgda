package pl.michal.zoo.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.michal.zoo.entity.Animal;
import pl.michal.zoo.util.HibernateUtil;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Lenovo on 12.07.2017.
 */
public class AnimalDAO implements AbstractDao<Animal>{
    public boolean insert(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        int check = Integer.valueOf(session.save(type) + "");
        t.commit();

        if(get(check) != null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        if(this.get(id) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean update(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        if(this.get(type.getId()).equals(type)){
            return true;
        }
        session.close();
        return false;
    }

    public Animal get(int id) {
        Session session = HibernateUtil.openSession();
        Animal animal;
        animal = session.load(Animal.class, id);
        session.close();
        return animal;
    }

    public List<Animal> get() {
        List<Animal> animals;
        Session session = HibernateUtil.openSession();
        animals = session.createQuery("from Animal").list();
        session.close();
        return animals;
    }
}
