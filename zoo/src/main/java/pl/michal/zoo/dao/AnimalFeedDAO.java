package pl.michal.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.michal.zoo.entity.AnimalFeed;
import pl.michal.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by Lenovo on 13.07.2017.
 */
public class AnimalFeedDAO implements AbstractDao<AnimalFeed> {
    public boolean insert(AnimalFeed type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        int check = Integer.valueOf(session.save(type) + "");
        t.commit();

        if(get(check) != null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(AnimalFeed type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        if(this.get(id) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean update(AnimalFeed type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        if(this.get(type.getId()).equals(type)){
            return true;
        }
        session.close();
        return false;
    }

    public AnimalFeed get(int id) {
        Session session = HibernateUtil.openSession();
        AnimalFeed animal;
        animal = session.load(AnimalFeed.class, id);
        session.close();
        return animal;
    }

    public List<AnimalFeed> get() {
        List<AnimalFeed> animals;
        Session session = HibernateUtil.openSession();
        animals = session.createQuery("from AnimalFeed ").list();
        session.close();
        return animals;
    }


}
