package pl.michal.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.michal.zoo.entity.Animal;
import pl.michal.zoo.entity.Staff;
import pl.michal.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by Lenovo on 12.07.2017.
 */
public class StaffDao implements AbstractDao<Staff> {


    public boolean insert(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        int check = Integer.valueOf(session.save(type) + "");
        t.commit();

        if(get(check) != null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        if(this.get(id) == null){
            return true;
        }
        session.close();
        return false;
    }

    public boolean update(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        if(this.get(type.getId()).equals(type)){
            return true;
        }
        session.close();
        return false;
    }

    public Staff get(int id) {
        Session session = HibernateUtil.openSession();
        Staff staff;
        staff = session.load(Staff.class, id);
        session.close();
        return staff;
    }

    public List get() {
        List<Staff> staff;
        Session session = HibernateUtil.openSession();
        staff = session.createQuery("from Staff").list();
        session.close();
        return staff;
    }
}
