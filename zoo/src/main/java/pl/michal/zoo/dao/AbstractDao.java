package pl.michal.zoo.dao;

import java.util.List;

/**
 * Created by Lenovo on 12.07.2017.
 */
public interface AbstractDao<T> {
    boolean insert(T type);
    boolean delete(T type);
    boolean delete(int id);
    boolean update(T type);
    T get(int id);
    List<T> get();


}
