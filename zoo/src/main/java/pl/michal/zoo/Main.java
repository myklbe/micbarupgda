package pl.michal.zoo;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.michal.zoo.dao.AnimalDAO;
import pl.michal.zoo.entity.Animal;
import pl.michal.zoo.util.HibernateUtil;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

/**
 * Created by Lenovo on 12.07.2017.
 */
public class Main {
    public static void main(String[] args) {
        AnimalDAO animalDao = new AnimalDAO();

        // BEGIN: insert
        Animal a = new Animal();
        a.setName("Słoń");
        animalDao.insert(a);
        animalDao.insert(new Animal("Orzel"));
        animalDao.insert(new Animal ("Gołąb"));
        // BEGIN: retrieve


        Animal slon = new Animal();
        slon = animalDao.get(1);

        System.out.println("Pobrales id=" + slon.getId() + " " + slon.getName());

        // BEGIN: update
        slon.setName("PrzerobionySlon");
        animalDao.update(slon);
        // BEGIN: delete
        animalDao.delete(3); //delete:golab
        Animal secondAnimal = new Animal();
        secondAnimal.setId(2);
        animalDao.delete(secondAnimal);
        // BEGIN: retrieve all

        for(Animal anim : animalDao.get()){
            System.out.println( anim.getId() + ". " + anim.getName());
        }
    }
}