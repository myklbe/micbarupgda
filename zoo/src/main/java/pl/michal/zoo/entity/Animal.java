package pl.michal.zoo.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lenovo on 12.07.2017.
 */
@Entity
@Table(name="animal")
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;
    @OneToMany(fetch = FetchType.LAZY)
    private Set<AnimalFeed> animalFeedSet = new HashSet<AnimalFeed>();

    public Animal () {}

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public Set<AnimalFeed> getAnimalFeedSet() {
        return animalFeedSet;
    }

    public void setAnimalFeedSet(Set<AnimalFeed> animalFeedSet) {
        this.animalFeedSet = animalFeedSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
