package pl.michal.zoo.entity;

import javax.persistence.*;

/**
 * Created by Lenovo on 13.07.2017.
 */
@Entity
@Table
public class AnimalFeed {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column (name = "who")
    private String who;
    @Column (name = "amount")
    private int amount;
    @ManyToOne(fetch = FetchType.LAZY)
    private Animal animal;

    public AnimalFeed(){}

    public AnimalFeed(String who, int amount, Animal animal) {
        this.who = who;
        this.amount = amount;
        this.animal = animal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
