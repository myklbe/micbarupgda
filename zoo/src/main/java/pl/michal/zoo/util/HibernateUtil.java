package pl.michal.zoo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Lenovo on 12.07.2017.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory(){
        return new Configuration().configure().buildSessionFactory();
    }
    private static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static Session openSession(){
        return getSessionFactory().openSession();
    }

}
