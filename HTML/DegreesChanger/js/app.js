var app = angular.module('termometer', []);

app.controller('change',['$scope', function($scope){
    
    $scope.transformC = function() {
    //postMessage($scope.transformCtoF);
        $scope.fah = $scope.cel * 1.8 + 32;
        $scope.kel = 273.15 + $scope.cel * 1;
    }
    $scope.transformF = function() {
        $scope.cel = ($scope.fah - 32) / 1.8;
        $scope.kel = ($scope.fah * 1 + 459.67) * 5 / 9;
    }
    $scope.transformK = function() {
        scope.cel = scope.kel - 273.15;
        scope.fah = scope.kel * 1.8 - 459.67;
    }

}]);