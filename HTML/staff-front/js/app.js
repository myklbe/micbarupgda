var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8080/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/addProfession', {
            templateUrl: path + 'addProfession.html',
            controller: 'addProfessionController'
        })
        .when('/addStaff', {
            templateUrl: path + 'addStaff.html',
            controller: 'addStaffController'
        })
        .when('/show', {
            templateUrl: path + 'staffList.html',
            controller: 'showStaffController'
        })
        .when('/staff/show/:id', {
            templateUrl: path + 'staff.html',
            controller: 'staffController'
        })
        .when('/profession/show', {
            templateUrl: path + 'ProfessionList.html',
            controller: 'showProffesionController'
        })
        .when('/profession/show/:id', {
            templateUrl: path + 'profession.html',
            controller: 'professionController'
    });
});

app.controller('addStaffController', function($scope, $http) {
    
    $http({
        url: url + 'staff/show',
        dataType: 'json'
    }).then(function(success) {
       $scope.staff = success.data;
    }, function(error) {
        console.error(error);
    });
    
    $http({
        url: url + 'profession/show',
        dataType: 'json'
    }).then(function(success) {
       $scope.profession = success.data;
    }, function(error) {
        console.error(error);
    });
    
    $scope.add = function() {
        $http({
            url: url + 'staff/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                lastname: $scope.lastname,
                salary: $scope.salary,
                profession: $scope.professions
            }
        }).then(function(success) {
            $scope.message = "success";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('addProfessionController', function($scope, $http) {
    
    $http({
       url: url + 'profession/show'
    }).then(function(success) {
       $scope.profession = success.data;
    }, function(error) {
        console.error(error);
    });
    
    $scope.add = function() {
        $http({
            url: url + 'profession/add',
            dataType: 'json',
            params: {
                name: $scope.name,
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.profession.push(success.data);
                $scope.message = "successfully added"
        } else 
                $scope.message = "Error error fire!";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showStaffController', function($scope, $http) {
    $http({
        url: url + 'staff/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.staff = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('staffController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'staff/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.staff = success.data;
    }, function(error) {
        console.error(error);
    });
});



app.controller('professionController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'profession/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.profession = success.data;
    }, function(error) {
        console.error(error);
    });
});