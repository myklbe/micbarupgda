<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:import url="header.jsp" />

<c:import url="sideBar.jsp" />

<div class="page">
    <p> ERROR! ${message} </p>
</div>


<c:import url="footer.jsp" />
