package pl.michal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("login");
        String pass = request.getParameter("password");

        List<String> colors = Arrays.asList("black", "blue", "yellow", "white");

        HashMap<String, String> users = new HashMap<String, String>();
        users.put("Paweł", "testowy");
        users.put("Michał", "testowawy");
        users.put("Grzes", "testowkowy");

        if(users.containsKey(name)){
            if(users.get(name).equals(pass)){
                request.setAttribute("name", name);
                request.setAttribute("colors", colors);
                request.getRequestDispatcher("druga.jsp").forward(request,response);
            }
            else {
                request.setAttribute("message", "Bad password");
                request.getRequestDispatcher("error.jsp").forward(request, response);
            }

        }else {
            request.setAttribute("message", "Bad login and password");
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }




    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
