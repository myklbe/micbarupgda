package calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum(){
		int a = 3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		//sc.add(a,b);
		assertEquals(9,sc.add(a,b));
		
	}
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception{
		SimpleCalc sc = new SimpleCalc();
		sc.exThrow();
	}
	@Test
	public void whenFisrtNumberIsNegativeAndSecondPossitive(){
		int a = -3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(3,sc.add(a,b));
		
	}
	@Test
	public void whenFirstIsPossitiveAndSecondNegative(){
		int a = 3, b = -6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-3,sc.add(a,b));
	
	}
	@Test
	public void whenBothNumbersAreNegative(){
		int a = -3, b = -6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-9,sc.add(a,b));
	}
	@Test
	public void whenTwoMaxIntegersAreGivenPossitiveNumberAreExpected(){
		SimpleCalc sc = new SimpleCalc();
		assertTrue("out of range", sc.add(Integer.MAX_VALUE, 5) > 0);
	}
}
