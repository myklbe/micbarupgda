package matrixTest;

import static org.junit.Assert.*;

import java.nio.channels.AsynchronousServerSocketChannel;

import org.junit.Test;

import matrix.Matrix;

public class MatrixMethodTest {

	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected() {
		int[][] m2 = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		Matrix mat = new Matrix();
		int[][] actual = new int[3][3];
		assertArrayEquals(m2, mat.identityMatrix(actual));
	}

	@Test
	public void whenActualArrayGivenIndexetMatrixExpected() {
		int[][] actual = new int[3][3];
		int[][] m2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		Matrix mat = new Matrix();
		assertArrayEquals(m2, mat.indexedMatrix(actual));
	}

	@Test
	public void wheActualArrayWithDifferendParametersGivenIndexetMatrixExpected() {
		int[][] actual = new int[2][3];
		int[][] m2 = { { 1, 2, 3 }, { 4, 5, 6 } };
		Matrix mat = new Matrix();
		assertArrayEquals(m2, mat.indexedMatrix(actual));
	}

	@Test
	public void whenExpectedArrayisEmptyElementDifferentExpected() {
		int[][] actual = new int[2][2];
		int[][] m2 = new int[2][2];
		Matrix mat = new Matrix();
		assertArrayEquals(m2, mat.indexedMatrix(actual));
	}

	@Test
	public void whenTwoSameArraysGivenTrueExpected() {
		int[][] first = new int[2][2];
		int[][] second = new int[2][2];
		Matrix mat = new Matrix();
		assertTrue(mat.isEqualDimension(first, second));
	}

	@Test
	public void whenTwoDifferentDimentionArraysGivenFalseExpected() {
		int[][] first = new int[1][2];
		int[][] second = new int[2][2];
		Matrix mat = new Matrix();
		assertFalse(mat.isEqualDimension(first, second));
	}

	@Test
	public void whenTwoArrayMatrixAreTheSameMatrixSumExpected() {
		int[][] m = { { 2, 2 }, { 2, 2 } };
		int[][] n = { { 2, 2 }, { 2, 2 } };
		Matrix mat = new Matrix();
		int[][] expected = { { 4, 4 }, { 4, 4 } };
		assertArrayEquals(expected, mat.addMatrix(n, m));
	}

	@Test
	public void whenArraysHaveDifferentSizesExceptionExpected() {
		int[][] m = { { 2, 2, 3 }, { 2, 2, 3 } };
		int[][] n = { { 2, 2 }, { 2, 2 } };
		Matrix mat = new Matrix();
		int[][] expected = { { 4, 4 }, { 4, 4 } };
		assertArrayEquals(expected, mat.addMatrix(n, m));
	}

	@Test
	public void whenArraysAreEmptyEmptyMatrixExpected() {
		int[][] m = new int[2][2];
		int[][] n = new int[2][2];
		Matrix mat = new Matrix();
		int[][] expected = { { 0, 0 }, { 0, 0 } };
		assertArrayEquals(expected, mat.addMatrix(n, m));
	}

	@Test
	public void whenTwoArrayHaveNegativeElementsNegativeMatrixSumExpected() {
		int[][] m = { { -2, -2 }, { -2, -2 } };
		int[][] n = { { -2, -2 }, { -2, -2 } };
		Matrix mat = new Matrix();
		int[][] expected = { { -4, -4 }, { -4, -4 } };
		assertArrayEquals(expected, mat.addMatrix(n, m));
	}
	

}
